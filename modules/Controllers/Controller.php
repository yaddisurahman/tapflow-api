<?php

namespace Modules\Controllers;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class Controller
{

  protected $logger;
  protected $tapdw;
  protected $tapflow;
  protected $query_helper;
  protected $guzzle;

  function __construct($container)
  {
    $this->logger = $container->logger;
    $this->tapdw = $container->tapdw;
    $this->tapflow = $container->tapflow;
    $this->staging = $container->staging;
    
    $this->query_helper = $container->query_helper;
    $this->guzzle = $container->guzzle;
    // $this->ldap = $container->ldap;
  }

  function __get($property) {
    if($this->container->{$property}) {
      return $this->container->{$property};
    }
  }
}
