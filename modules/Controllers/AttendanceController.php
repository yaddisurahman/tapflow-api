<?php

/**
 * Deployment Notes:
 * DEVDW_LINK diupdate menjadi PRODDW_LINK
 */
namespace Modules\Controllers;

class AttendanceController extends Controller
{

  public function index($req, $res)
  {
    
  }

  /**
   * Tampilkan data ketidak hadiran karyawan berdasarkan BRD Terminasi Karyawan, halaman 9
   * @param  [type] $req [description]
   * @param  [type] $res [description]
   * @param  [type] $arg [description]
   * @return [type]      [description]
   */
  public function getUnpresent($req, $res, $arg)
  {
    $res->withHeader('Content-type', 'application/json');
    if(!$arg['ref_date_start'] || !$arg["ref_month"]) {
      return $res->withJson(['error' => true, 'status' => 500, 'message' => 'Parameter not satisfied']);
    }

    $date_start = date($arg["ref_date_start"]);
    $ref_month = $arg["ref_month"];
    
    $date_start = strtoupper(date('d-M-y', strtotime($date_start)));
    $ref_start = $ref_month * 30;
    $ref_end   = ($ref_month * 30) + 29;

    $where_present_date = "BETWEEN TO_DATE('$date_start') - NUMTODSINTERVAL('$ref_end','DAY') AND TO_DATE('$date_start') - NUMTODSINTERVAL('$ref_start','DAY')";
    
    if( isset($arg['max']) && strtolower($arg['max']) == 'true' ) {
      $where_present_date = "< TO_DATE('$date_start') - NUMTODSINTERVAL('$ref_start','DAY')";
    }
    if(isset($arg["status"])) {
      $employee_status = $arg["status"];
      $where_present_date .= ' AND '. $this->query_helper->generateFilters(null, $employee_status, 'EMP.STATUS');
      $where_present_date = substr($where_present_date, 0, -4);
    }

    $instance_search = ["NIK_SAP" => "UPPER(EE.NIK_SAP)","EMPLOYEE_NAME" => "UPPER(EE.EMPLOYEE_NAME)","NIK_NASIONAL" => "UPPER(EE.NIK_NASIONAL)","JOB_CODE" => "UPPER(EE.JOB_CODE)","JOB_TYPE" => "UPPER(EE.JOB_TYPE)","WERKS" => "UPPER(AC.AREA_CODE)","AREA_CODE" => "UPPER(AC.AREA_CODE)", "STATUS" => "UPPER(EE.STATUS)"];

    $where_ins = ''; 
    foreach ($arg as $key => $val) {
      if (array_key_exists($key, $instance_search)) {
        $where_ins .= 'AND '. $this->query_helper->generateFilters($key, $val, $instance_search[$key]);
        $where_ins = substr($where_ins, 0, -4);
      }
    }

    // Kalau deploy ke production, DEVDW_LINK diupdate menjadi PRODDW_LINK
    $sql = "SELECT ATT.ATTENDANCE_DATE, EMP.* FROM TM_EMPLOYEE_LAST_ATTENDANCE@DEVDW_LINK ATT
            JOIN ( SELECT EE.NIK_SAP, EE.NIK_NASIONAL, EE.EMPLOYEE_NAME, EE.PROF_NAME, AC.AREA_CODE, EE.JOB_CODE, EE.JOB_TYPE, EE.STATUS
            FROM TM_EMPLOYEE_PERSONALIA EE
            JOIN TM_AREA_CODE@DEVDW_LINK AC ON AC.PAYROLL = EE.PROF_NAME
            WHERE EE.RES_DATE IS NULL AND TRUNC(TO_DATE('".date('Y-m-d')."', 'YYYY-MM-DD'), 'MM') BETWEEN EE.START_VALID AND
              CASE WHEN EXTRACT (YEAR FROM EE.RES_DATE) != 9999 THEN EE.RES_DATE ELSE EE.END_VALID END $where_ins
            ) EMP ON EMP.NIK_SAP = ATT.NIK
            WHERE ATT.ATTENDANCE_CODE LIKE 'K%' 
            AND ATT.ATTENDANCE_DATE $where_present_date";

    try {
      $ps = $this->tapflow->query($sql);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'][] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $this->logger->debug('getUnpresent :'. str_replace('  ', '', $sql) );
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }
    $this->logger->debug('getUnpresent :'. str_replace('  ', '', $sql) );
    return $res->withJson($result);
  }

  /**
   * Tampilkan data ketidak hadiran karyawan berdasarkan BRD Terminasi Karyawan, halaman 9
   * @param  [type] $req [description]
   * @param  [type] $res [description]
   * @param  [type] $arg [description]
   * @return [type]      [description]
   */
  public function getLastActivity($req, $res, $arg)
  {
    $res->withHeader('Content-type', 'application/json');
    if(!$arg['ref_date_start'] || !$arg["ref_month"]) {
      return $res->withJson(['error' => true, 'status' => 500, 'message' => 'Parameter not satisfied']);
    }

    $date_start = date($arg["ref_date_start"]);
    $ref_month = $arg["ref_month"];
    
    $date_start = date('d-M-y', strtotime("-1 month", strtotime($date_start)) ); 
    $date_end = date('d-M-y', strtotime("-$ref_month-1 month", strtotime($date_start)) );

    $where_present_date = "BETWEEN '$date_end' AND '$date_start'";
    if( isset($arg['max']) && $arg['max'] == 'true' ) {
      $where_present_date = " < '$date_end' ";
    }

    $sql = "SELECT ATT.ATTENDANCE_DATE, EMP.NIK, EMP.EMPLOYEE_NAME, EMP.JOB_CODE, EMP.JOB_TYPE,
            EMP.STATUS, EMP.WERKS, EMP.PROF_NAME 
            FROM TM_EMPLOYEE_LAST_YEAR_ATTD ATT
            JOIN ( SELECT * FROM TM_EMPLOYEE_SAP WHERE TRUNC(SYSDATE, 'MM') BETWEEN START_VALID AND
              CASE WHEN EXTRACT (YEAR FROM RES_DATE) != 9999 THEN RES_DATE ELSE END_VALID END
            ) EMP ON EMP.NIK = ATT.NIK
            WHERE ATT.ATTENDANCE_DATE = 
            ( SELECT MAX(M.ATTENDANCE_DATE) FROM TM_EMPLOYEE_LAST_YEAR_ATTD M 
              WHERE M.NIK = ATT.NIK ) AND ATT.ATTENDANCE_CODE LIKE 'K%' 
            AND ATT.ATTENDANCE_DATE $where_present_date";

    try {
      $ps = $this->tapdw->query($sql);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'][] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }
    return $res->withJson($result);
  }

}
