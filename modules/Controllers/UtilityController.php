<?php

namespace Modules\Controllers;

class UtilityController extends Controller
{

  public function index($req, $res)
  {
    $res->withHeader('Content-type', 'application/json');
    return $res->withJson(['error' => true, 'status' => 404, 'message' => 'No parameter provided']);
  }
  /**
   * Get COA Code
   */
  public function getCoa($req, $res, $arg)
  {
    $column_name = $this->query_helper->getColumnName('TM_COA', $this->tapdw);
    $selected_fields = implode(',', $column_name);

    $where = "where lower(LANG_KEY) in ('i', 'e') ";
    foreach ($column_name as $key) {
      $search_param[$key] = 'UPPER('.$key.')';
    }

    foreach ($arg as $key => $val) {
      if (array_key_exists($key, $search_param)) {
        $where .= 'AND '.$this->query_helper->generateFilters($key, $val, $search_param[$key]);
        $where = substr($where, 0, -4);
      }
    }

    $limit = (isset($arg['LIMIT'])) ? $arg['LIMIT'] : 500;
    $offset = (isset($arg['OFFSET'])) ? $arg['OFFSET'] : 0;
    $offset_limit = " OFFSET $offset ROWS FETCH NEXT $limit ROWS ONLY";
    // $q = "SELECT $selected_fields FROM TM_COA $where $offset_limit";
    $q = "SELECT $selected_fields FROM TM_COA $where";

    $time_start = microtime(true);
    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'][] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }
    $time_end = microtime(true);
    $this->logger->debug('getCoa :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );

    return $res->withJson($result);
  }

  public function bank($req, $res, $arg)
  {
    $search_key = array("BANK_ID" => "BANK_ID", "BANK_NAME" => "BANK_NAME");

    $where = '';
    foreach ($arg as $key => $val) {
      if (array_key_exists($key, $search_key)) {
        $where .= $search_key[$key]." like '%".strtoupper($val)."%' AND ";
      }
    }
    if($where) {
      $where = 'AND '.substr($where, 0, -4);
    }

    $q = "SELECT BANK_ID, BANK_NAME FROM TM_BANK WHERE LENGTH(BANK_ID) = 3 $where ORDER BY BANK_NAME";

    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'][] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    $res->withHeader('Content-type', 'application/json');
    return $res->withJson($result);
  }

  public function getBank($req, $res, $arg)
  {
    if(!$arg['BANK_ID']) {
      return $res->withJson(['error' => true, 'status' => 500, 'message' => 'Parameter not satisfied']);
    }

    $q = "SELECT BANK_ID, BANK_NAME FROM TM_BANK WHERE BANK_ID = '".$arg['BANK_ID']."'";

    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    return $res->withJson($result);
  }

  /**
   * Get general parameter data
   */
  public function getData($req, $res, $arg)
  {
    $search_key = ["code" => "GENERAL_CODE", "value" => "DESCRIPTION_CODE", "label" => "DESCRIPTION"];

    $where = '';
    foreach ($arg as $key => $val) {
      $where .= $search_key[$key]." like '%".strtoupper($val)."%' AND ";
    }
    if($where) {
      $where = 'WHERE '.substr($where, 0, -4);
    }

    $q = "SELECT GENERAL_CODE, DESCRIPTION_CODE, DESCRIPTION FROM TM_GENERAL_DATA $where";

    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'][] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    return $res->withJson($result);
  }

}
