<?php 

namespace Modules\Controllers;

class EmailController extends Controller {

  /**
   * [index description]
   * @param  [type] $req [description]
   * @param  [type] $res [description]
   * @return [type]      [description]
   */
  public function index($req, $res)
  {
    $url = $req->getParam('email_url');

    // $url = 'http://tap-workflow.tap-agri.com/get-email-complete-pdm/MTcuMDYvSEMvUERNLU5TLzAwMjY0fFBHQXxmcmFucy5sdWJpc3wzMjE=';
    // $param  = base64_encode('17.06/HC/PDA-NS/00076|KTU|amri.siregar|269');
    // $url    = 'http://tap-workflow.tap-agri.com/get-email-template-pdpk-approval/'.$param;

    /**
     * Ketika mengambil email berdasarkan url, parameter yang direturn harus ada
     * @return [array] ['subject', 'sender', 'receiver', 'email_body']
     *         optiona ['reply_to', 'attachment']
     */
    $res = $this->guzzle->request('GET', $url);

    $email_template = json_decode($res->getBody());

    foreach ($email_template->data as $template) {
      $message = \Swift_Message::newInstance()
                 ->setSubject('Your subject')
                 ->setFrom(array('no-reply@tap-agri.com' => 'TAP Flow Application'))
                 ->setTo(array('yaddi.surahman@tap-agri.co.id'))
                 ->setBody($template->content, 'text/html');


      $transport = \Swift_SmtpTransport::newInstance()
                  ->setHost('smtp.tap-agri.com')
                  ->setPort(25);

      $mailer = \Swift_Mailer::newInstance($transport);
      $result = $mailer->send($message);
    }
  }


  public function sendEmail($req, $res)
  {

    $email_components = $req->getParsedBody();
    $this->logger->debug('email :'. json_encode($req->getParsedBody()) );

    $message = \Swift_Message::newInstance()
               ->setSubject($email_components['subject'])
               ->setFrom(array('no-reply@tap-agri.com' => 'Fertilizer Tracking Application'))
               ->setTo($email_components['send_to'])
               ->setBody($email_components['email_body'], 'text/html');

    $transport = \Swift_SmtpTransport::newInstance()
                ->setHost('smtp.tap-agri.com')
                ->setPort(25);

    $mailer = \Swift_Mailer::newInstance($transport);
    $result = $mailer->send($message);
  }


}
