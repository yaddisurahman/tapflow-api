<?php 

namespace Modules\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Modules\Models\HsModel;

class AreaController extends Controller {

  public function index(ServerRequestInterface $req, ResponseInterface $res)
  {
    $res->withHeader('Content-type', 'application/json');
    return $res->withJson(['error' => true, 'status' => 404, 'message' => 'No parameter provided']);
  }

  public function region(ServerRequestInterface $req, ResponseInterface $res, $arg)
  {
    $search_key = ["REGION_CODE" => "UPPER(REGION_CODE)", "REGION_NAME" => "UPPER(REGION_NAME)"];
    $where = '';
    foreach ($arg as $key => $val) {
      if(strpos($val, '|')) {
        $val = explode('|', $val);
      }

      if (array_key_exists($key, $search_key)) {
        if (is_array($val)) {
          $where .= $search_key[$key]." in ('".strtoupper(implode("','", $val))."') AND ";
        } else {
          $where .= $search_key[$key]." like '%".strtoupper($val)."%' AND ";
        }
      }
    }

    if($where) {
      $where = 'WHERE '.substr($where, 0, -4);
    }

    $q = "SELECT REGION_CODE, REGION_NAME FROM TM_REGION $where";

    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'][] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    $res->withHeader('Content-type', 'application/json');
    return $res->withJson($result);
  }

  public function getRegion(ServerRequestInterface $req, ResponseInterface $res, $arg)
  {
    $res->withHeader('Content-type', 'application/json');
    if(!$arg['REGION_CODE']) {
      return $res->withJson(['error' => true, 'status' => 500, 'message' => 'Parameter not satisfied']);
    }

    $q = "SELECT REGION_CODE, REGION_NAME FROM TM_REGION WHERE REGION_CODE = '".$arg['REGION_CODE']."'";

    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    return $res->withJson($result);
  }

  public function company(ServerRequestInterface $req, ResponseInterface $res, $arg)
  {
    $search_key = ["REGION_CODE" => "UPPER(RR.REGION_CODE)", "REGION_NAME" => "UPPER(RR.REGION_NAME)", 
    "COMP_CODE" => "UPPER(CC.COMP_CODE)", "COMP_NAME" => "UPPER(CC.COMP_NAME)"];
    $where = '';
    foreach ($arg as $key => $val) {
      if(strpos($val, '|')) {
        $val = explode('|', $val);
      }

      if (array_key_exists($key, $search_key)) {
        if (is_array($val)) {
          $where .= $search_key[$key]." in ('".strtoupper(implode("','", $val))."') AND ";
        } else {
          $where .= $search_key[$key]." like '%".strtoupper($val)."%' AND ";
        }
      }
    }

    if($where) {
      $where = 'WHERE '.substr($where, 0, -4);
    }

    $q = "SELECT RR.REGION_CODE, RR.REGION_NAME, CC.COMP_CODE, CC.COMP_NAME 
          FROM TM_COMP CC JOIN TM_REGION RR ON RR.REGION_CODE = CC.REGION_CODE $where";

    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'][] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    $res->withHeader('Content-type', 'application/json');
    return $res->withJson($result);
  }

  public function getCompany(ServerRequestInterface $req, ResponseInterface $res, $arg)
  {
    $res->withHeader('Content-type', 'application/json');
    if(!$arg['COMP_CODE']) {
      return $res->withJson(['error' => true, 'status' => 500, 'message' => 'Parameter not satisfied']);
    }

    $q = "SELECT RR.REGION_CODE, RR.REGION_NAME, CC.COMP_CODE, CC.COMP_NAME FROM TM_COMP CC 
          JOIN TM_REGION RR ON RR.REGION_CODE = CC.REGION_CODE WHERE COMP_CODE = '".$arg['COMP_CODE']."'";

    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    return $res->withJson($result);
  }

  public function estate(ServerRequestInterface $req, ResponseInterface $res, $arg)
  {
    $search_key = ["REGION_CODE" => "UPPER(RR.REGION_CODE)", "COMP_CODE" => "UPPER(CC.COMP_CODE)", "EST_CODE" => "UPPER(EE.EST_CODE)", "WERKS" => "UPPER(EE.WERKS)", "EST_NAME" => "UPPER(AC.PAYROLL)"];
    
    $where = '';
    foreach ($arg as $key => $val) {
      if(strpos($val, '|')) {
        $val = explode('|', $val);
      }

      if (array_key_exists($key, $search_key)) {
        if (is_array($val)) {
          $where .= $search_key[$key]." in ('".strtoupper(implode("','", $val))."') AND ";
        } else {
          $where .= $search_key[$key]." like '%".strtoupper($val)."%' AND ";
        }
      }
    }

    if($where) {
      $where = 'WHERE '.substr($where, 0, -4);
    }

    $q = "SELECT RR.REGION_CODE, RR.REGION_NAME, CC.COMP_CODE,CC.COMP_NAME,EE.WERKS,
          EE.EST_CODE, AC.PAYROLL AS EST_NAME 
          FROM TM_EST EE JOIN TM_REGION RR ON RR.REGION_CODE = EE.REGION_CODE
          JOIN TM_COMP CC ON CC.COMP_CODE = EE.COMP_CODE
          LEFT JOIN TM_AREA_CODE AC ON AC.AREA_CODE = EE.WERKS $where";

    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'][] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }
    $this->logger->debug('estate :'. str_replace('  ', '', $q) );
    $res->withHeader('Content-type', 'application/json');
    return $res->withJson($result);
  }

  public function getEstate(ServerRequestInterface $req, ResponseInterface $res, $arg)
  {
    $res->withHeader('Content-type', 'application/json');
    if(!$arg['COMP_CODE'] || !$arg['EST_CODE']) {
      return $res->withJson(['error' => true, 'status' => 500, 'message' => 'Parameter not satisfied']);
    }
    $q = "SELECT RR.REGION_CODE, RR.REGION_NAME, CC.COMP_CODE,CC.COMP_NAME,EE.WERKS,EE.EST_CODE, EE.EST_NAME
          FROM TM_EST EE JOIN TM_REGION RR ON RR.REGION_CODE = EE.REGION_CODE
          JOIN TM_COMP CC ON CC.COMP_CODE = EE.COMP_CODE WHERE EE.COMP_CODE = '".$arg['COMP_CODE']."' 
          AND EE.EST_CODE = '".$arg['EST_CODE']."'";

    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    return $res->withJson($result);
  }

  public function afdeling(ServerRequestInterface $req, ResponseInterface $res, $arg)
  {
    $search_key = ["REGION_CODE" => "UPPER(REGION_CODE)", "COMP_CODE" => "UPPER(COMP_CODE)", "EST_CODE" => "UPPER(EST_CODE)", "WERKS" => "UPPER(WERKS)", "SUB_BA_CODE" => "UPPER(SUB_BA_CODE)", "AFD_CODE" => "UPPER(AFD_CODE)", "AFD_NAME" => "UPPER(AFD_NAME)"];
    $where = '';
    foreach ($arg as $key => $val) {
      if(strpos($val, '|')) {
        $val = explode('|', $val);
      }

      if (array_key_exists($key, $search_key)) {
        if (is_array($val)) {
          $where .= $search_key[$key]." in ('".strtoupper(implode("','", $val))."') AND ";
        } else {
          $where .= $search_key[$key]." like '%".strtoupper($val)."%' AND ";
        }
      }
    }

    if($where) {
      $where = 'WHERE '.substr($where, 0, -4);
    }

    $q = "SELECT REGION_CODE, COMP_CODE, EST_CODE, WERKS, SUB_BA_CODE, AFD_CODE, UPPER(AFD_NAME) AFD_NAME FROM TM_AFD $where ORDER BY WERKS, 7";

    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'][] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    $res->withHeader('Content-type', 'application/json');
    return $res->withJson($result);
  }

  public function getAfdeling(ServerRequestInterface $req, ResponseInterface $res, $arg)
  {
    $res->withHeader('Content-type', 'application/json');
    if(!$arg['EST_CODE'] || !$arg['AFD_CODE']) {
      return $res->withJson(['error' => true, 'status' => 500, 'message' => 'Parameter not satisfied']);
    }
    $q = "SELECT REGION_CODE, COMP_CODE, EST_CODE, WERKS, SUB_BA_CODE, AFD_CODE, AFD_NAME FROM TM_AFD WHERE EST_CODE = '".$arg['EST_CODE']."' AND AFD_CODE = '".$arg['AFD_CODE']."'";

    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    return $res->withJson($result);
  }

  public function block(ServerRequestInterface $req, ResponseInterface $res, $arg)
  {
    $search_key = ["REGION_CODE" => "UPPER(REGION_CODE)", "COMP_CODE" => "UPPER(COMP_CODE)", "EST_CODE" => "UPPER(EST_CODE)", "WERKS" => "UPPER(WERKS)", "SUB_BA_CODE" => "UPPER(SUB_BA_CODE)", "AFD_CODE" => "UPPER(AFD_CODE)", "AFD_NAME" => "UPPER(AFD_NAME)", "BLOCK_CODE" => "UPPER(BLOCK_CODE)", "BLOCK_NAME" => "UPPER(BLOCK_NAME)"];

    $where = '';
    foreach ($arg as $key => $val) {
      if (array_key_exists($key, $search_key)) {
        $where .= $search_key[$key]." like '%".strtoupper($val)."%' AND ";
      }
    }
    if($where) {
      $where = 'AND '.substr($where, 0, -4);
    }

    $q = "SELECT REGION_CODE, COMP_CODE, EST_CODE, WERKS, SUB_BA_CODE,
          AFD_CODE, AFD_NAME, BLOCK_CODE, BLOCK_NAME, START_VALID, END_VALID
          FROM TM_BLOCK WHERE TRUNC(TO_DATE('".date('Y-m-d')."', 'YYYY-MM-DD'), 'MM') BETWEEN START_VALID AND END_VALID $where"; 

    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'][] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    $res->withHeader( 'Content-Type', 'application/json' );
    return $res->withJson($result);
  }

  public function getBlock($req, $res, $arg)
  {
    $res->withHeader('Content-type', 'application/json');
    if(!isset($arg['COMP_CODE']) || !isset($arg['EST_CODE']) ||
       !isset($arg['AFD_CODE']) || !isset($arg['BLOCK_CODE']) ) {
      return $res->withJson(['error' => true, 'status' => 500, 'message' => 'Parameter not satisfied']);
    }

    $where = "AND COMP_CODE = upper('".$arg['COMP_CODE']."')
              AND EST_CODE = upper('".$arg['EST_CODE']."')
              AND AFD_CODE = upper('".$arg['AFD_CODE']."')
              AND BLOCK_CODE = upper('".$arg['BLOCK_CODE']."')";

    $q = "SELECT REGION_CODE, COMP_CODE, EST_CODE, WERKS, SUB_BA_CODE,
          AFD_CODE, AFD_NAME, BLOCK_CODE, BLOCK_NAME, START_VALID, END_VALID
          FROM TM_BLOCK WHERE TRUNC(TO_DATE('".date('Y-m-d')."', 'YYYY-MM-DD'), 'MM') BETWEEN START_VALID AND END_VALID $where"; 

    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    $res->withHeader('Content-type', 'application/json');
    return $res->withJson($result);
  }


  public function baProfile($req, $res, $arg)
  {
    $ba_code       = $arg['ba_code']; // 4121  => comp_code + est_code (werks)
    $year_month    = $arg['year_month']; // current_date  yyyy-mm-dd
    $level_jabatan = $arg['level_jabatan'];
    $jabatan       = $arg['jabatan'];

    $res->withHeader('Content-type', 'application/json');
    if(!$arg['ba_code'] || !$arg['level_jabatan'] || !$arg['year_month']) {
      return $res->withJson(['error' => true, 'status' => 500, 'message' => 'Parameter not satisfied']);
    }

    $mandor_panen = ["operator" => "=", "prefix" => false, "suffix" => false, "job_code" => "'MANDOR PANEN'"];
    $mandor_rawat = ["operator" => "=", "prefix" => false, "suffix" => false, "job_code" => "'MANDOR RAWAT'"];
    // $mandor_tanam = ["operator" => "=", "prefix" => false, "suffix" => false, "job_code" => "'MANDOR TANAM'"];
    $mandor_1     = ["operator" => "=", "prefix" => false, "suffix" => false, "job_code" => "'MANDOR 1'"];
    $mandor_umum  = ["operator" => "not in", "prefix" => "(", "suffix" => ")", "job_code" => "'MANDOR PANEN', 'MANDOR RAWAT', 'MANDOR TANAM', 'MANDOR 1'"];

    $p_panen    = ["operator" => "=", "prefix" => false, "suffix" => false, "job_code" => "'PEMANEN'"];
    $p_rawat    = ["operator" => "=", "prefix" => false, "suffix" => false, "job_code" => "'PEKERJA RAWAT'"];
    $p_umum     = ["operator" => "not in", "prefix" => "(", "suffix" => ")", "job_code" => "'PEMANEN', 'PEKERJA RAWAT'"];
    $p_general  = ["operator" => "=", "prefix" => false, "suffix" => false, "job_code" => "'".$jabatan."'"];

    $c = ['tapdw' => $this->tapdw, 'logger' => $this->logger];
    $hs = new HsModel($c);

    switch ($level_jabatan) {
      case 'ROS': // ROF REGIONAL OFFICE - Staff
          $result['desc']          = 'Area Estate REGIONAL OFFICE, padahal request jabatannya '. $jabatan;
          // $mpe = $hs->getMpeBasedOnJob($ba_code, $year_month, $p_general);
          $result['mpe'] = intval(0);
        break;

      case 'RON': // ROF REGIONAL OFFICE - Non Staff
          $result['desc']          = 'Area Estate REGIONAL OFFICE, padahal request jabatannya '. $jabatan;
          $mpe = $hs->getMpeBasedOnJob($ba_code, $year_month, $p_general);
          $result['mpe'] = array_sum($mpe);
        break;

      case 'EST': // EST ESTATE STAFF
          $result['desc']          = 'Area Estate ESTATE STAFF, padahal request jabatannya '. $jabatan;
          $result['ha_tanam']      = $hs->getHaTanam($ba_code, $year_month);
          $result['ha_tm']         = $hs->getTmTbm($ba_code, $year_month, 'TM');
          $result['ha_tbm']        = $hs->getTmTbm($ba_code, $year_month, 'TBM');
          $mpe = $hs->getMpeBasedOnJob($ba_code, $year_month, $p_general);
          if(array_keys($mpe)[0] == $jabatan) {
            $result['mpe'] = intval(0);
          } else {
            $result['mpe'] = $mpe;
          }
        break;

      case 'ENS': // ENS ESTATE NON STAFF
        if(strpos($jabatan, 'MANDOR') !== false) { //UI-PTK-06 : Area Estate Non-Staff Mandor
          // print_r($result);
          // die();
          $result['desc']          = 'Area Estate Non-Staff Mandor, request jabatannya '. $jabatan;
          $result['ha_tanam']      = $hs->getHaTanam($ba_code, $year_month);
          $result['ha_tm']         = $hs->getTmTbm($ba_code, $year_month, 'TM');
          $result['ha_panen']      = $hs->getHaPanen($ba_code, $year_month);
          $result['produksi']      = $hs->getProduksi($ba_code, $year_month);
          $result['bbc']           = $hs->getBBC($ba_code, $year_month);
          $mpe = $hs->getMpeBasedOnJob($ba_code, $year_month, $p_general);
          $result['mpe'] = array_sum($mpe);
          $mandor                  = $hs->getMandor($ba_code, $year_month);
          $pekerja                 = $hs->getMpePekerja($ba_code, $year_month);
          foreach ($mandor as $key => $val) { $result[str_replace(' ', '_', strtolower($key))] = $val; }
          if(!isset($result['mandor_panen'])) { $result['mandor_panen'] = intval(0); }
          if(!isset($result['mandor_rawat'])) { $result['mandor_rawat'] = intval(0); }
          if(!isset($result['mandor_umum'])) { $result['mandor_umum']   = intval(0); }
          if(!isset($result['mandor_1'])) { $result['mandor_1']         = intval(0); }
          
          foreach ($pekerja as $key => $val) {
            if($key == 'PEMANEN') {
              $key = 'PEKERJA PANEN';
            }
            $result[str_replace(' ', '_', strtolower($key))] = $val;
          }
          if(!isset($result['pekerja_panen'])) { $result['pekerja_panen'] = intval(0); }
          if(!isset($result['pekerja_rawat'])) { $result['pekerja_rawat'] = intval(0); }
          if(!isset($result['pekerja_umum'])) { $result['pekerja_umum']   = intval(0); }
          // $result['mandor_panen']  = $hs->getMpeBasedOnJob($ba_code, $year_month, $mandor_panen);
          // $result['mandor_rawat']  = $hs->getMpeBasedOnJob($ba_code, $year_month, $mandor_rawat);
          // $result['mandor_tanam']  = $hs->getMpeBasedOnJob($ba_code, $year_month, $mandor_tanam);
          // $result['mandor_umum']   = $hs->getMpemandorUmum($ba_code, $year_month);
          // $result['pekerja_panen'] = $hs->getMpeBasedOnJob($ba_code, $year_month, $p_panen);
          // $result['pekerja_rawat'] = $hs->getMpeBasedOnJob($ba_code, $year_month, $p_rawat);
          // $result['pekerja_umum']  = $hs->getMpePekerjaUmum($ba_code, $year_month);
          // $result['']
        }

        if(strpos($jabatan, 'MANDOR') === false && strpos($jabatan, 'PEMANEN') === false) { // UI-PTK-05 : Area Estate Non-Staff Rawat dan Umum 
          $result['desc']            = 'Area Estate Non-Staff Rawat dan Umum, request jabatannya '. $jabatan;
          $result['ha_tanam']        = $hs->getHaTanam($ba_code, $year_month);
          $result['ha_tm']           = $hs->getTmTbm($ba_code, $year_month, 'TM');
          $result['ha_tbm']          = $hs->getTmTbm($ba_code, $year_month, 'TBM');
          $result['kehadiran_mpe']   = $hs->getKehadiran($ba_code, $year_month, $jabatan);
          $result['kehadiran_ktkk']  = $hs->getKehadiranBasedOnStatus($ba_code, $year_month, $jabatan, array('KT', 'KK'));
          $result['kehadiran_mpekl'] = $hs->getKehadiranBasedOnStatus($ba_code, $year_month, $jabatan, array('KL'));
          // $result['mpe']             = $hs->getMpeBasedOnJob($ba_code, $year_month, $p_general);
          $mpe = $hs->getMpeBasedOnJob($ba_code, $year_month, $p_general);
          $result['mpe'] = array_sum($mpe);
          $mpe['KT'] =  (isset($mpe['KT'])) ? $mpe['KT'] :  0;
          $mpe['KL'] =  (isset($mpe['KL'])) ? $mpe['KL'] :  0;
          $mpe['KK'] =  (isset($mpe['KK'])) ? $mpe['KK'] :  0;
          $result['ktkk']= ($mpe['KT'] + $mpe['KK'] != 0)? $mpe['KT'] + $mpe['KK'] : intval(0) ;
          $result['mpekl']= ($mpe['KL'] != 0)? $mpe['KL'] : intval(0) ;
        }

        if($jabatan == 'PEMANEN') { //UI-PTK-04 : Area Estate Non-Staff Panen
          $result['desc']            = 'Area Estate Non-Staff Panen, request jabatannya '. $jabatan;
          $result['ha_tanam']        = $hs->getHaTanam($ba_code, $year_month);
          $result['ha_tm']           = $hs->getTmTbm($ba_code, $year_month, 'TM');
          $result['ha_panen']        = $hs->getHaPanen($ba_code, $year_month);
          $result['produksi']        = $hs->getProduksi($ba_code, $year_month);
          $result['bbc']             = $hs->getBBC($ba_code, $year_month);
          $result['mpe']             = $hs->getMPE($ba_code, $year_month);
          $result['ktkk']            = $hs->getPemanenBasedOnStatus($ba_code, $year_month, array('KT', 'KK'));
          $result['mpekl']           = $hs->getPemanenBasedOnStatus($ba_code, $year_month, array('KL'));
          $result['kehadiran_mpe']   = $hs->getKehadiran($ba_code, $year_month, $jabatan);
          $result['kehadiran_ktkk']  = $hs->getKehadiranBasedOnStatus($ba_code, $year_month, $jabatan, array('KT', 'KK'));
          $result['kehadiran_mpekl'] = $hs->getKehadiranBasedOnStatus($ba_code, $year_month, $jabatan, array('KL'));
          $result['produktifitas']   = $hs->getProduktifitas($ba_code, $year_month);
        }

        break;

      case 'MST': // MST MILL STAFF
          $result['desc']            = 'Area Estate MST MILL STAFF, padahal request jabatannya '. $jabatan;
          // $mpe = $hs->getMpeBasedOnJob($ba_code, $year_month, $p_general);
          $result['mpe'] = intval(0);
        break;

      case 'MNS': // MNS MILL NON STAFF
          $result['desc']            = 'Area Estate MILL NON STAFF, padahal request jabatannya '. $jabatan;
          $mpe = $hs->getMpeBasedOnJob($ba_code, $year_month, $p_general);
          $result['mpe'] = array_sum($mpe);
          $result['kehadiran_mpe']   = $hs->getKehadiran($ba_code, $year_month, $jabatan);
          $result['lembur_mpe']      = $hs->getOvertimeBasedOnJobcode($ba_code, $year_month, $jabatan);
          $result['lembur_karyawan'] = $hs->getOvertime($ba_code, $year_month);
        break;

    }
    return $res->withJson($result);
  }

}
