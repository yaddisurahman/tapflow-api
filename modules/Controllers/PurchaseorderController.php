<?php
namespace Modules\Controllers;

class PurchaseorderController extends Controller
{
	public function index(ServerRequestInterface $req, ResponseInterface $res)
  {
    $res->withHeader('Content-type', 'application/json');
    return $res->withJson(['error' => true, 'status' => 404, 'message' => 'No parameter provided']);
  }


   public function getPo($req, $res, $arg){
   		// $column_name = $this->query_helper->getColumnName('EKKO_SAP', $this->staging);
   		// $selected_fields = implode(',', $column_name);
   		 $search_key = array("EBELN" => "KO.EBELN","BUKRS" => "KO.BUKRS");
   		/* $where = "where KO.BSTYP = 'F' AND KO.LOEKZ =' '  AND 
   		 		   KO.bedat >= '20150101' AND SUBSTR(PO.MATNR,10,1) != '5%' AND KO.BSAKZ != 'T' AND "; */

   		$where = "where KO.BSTYP = 'F' AND 
   		 		   KO.bedat >= '20150101' AND KO.BSAKZ != 'T' AND ";
   		 
   
    	foreach ($arg as $key => $val) {
		      if (array_key_exists($key, $search_key)) {
		        $where .= $search_key[$key]." like '%".strtoupper($val)."%' AND ";
		      }
    	}

    	$where = substr($where, 0, -5);
    	

    	$limit = (isset($arg['LIMIT'])) ? $arg['LIMIT'] : 500;
	    $offset = (isset($arg['OFFSET'])) ? $arg['OFFSET'] : 0;
	    $offset_limit = " OFFSET $offset ROWS FETCH NEXT $limit ROWS ONLY";

	    $q = "SELECT KO.EBELN,KO.BSART,KO.BUKRS,PO.WERKS,KO.LIFNR,KO.BEDAT,KO.INCO2,KO.WAERS,KO.WKURS,KO.ZTERM,PO.MWSKZ,
    (select LF.name1 from lfa1_sap LF where LF.lifnr = KO.LIFNR) as NAME1
    FROM EKKO_SAP KO 
    LEFT JOIN EKPO_SAP PO ON
        KO.EBELN = PO.EBELN $where "; 
	    	


	    $time_start = microtime(true);
	    try {
	      $ps = $this->staging->query($q);
	      $ps->execute();

	      $data_displayed = 0;
	      while($row = $ps->fetch()) {
	        $result['data'][] = $row;
	        $data_displayed ++;
	      }
	      $result['count'] = $data_displayed;
	    } catch (\Exception $e) {
	      $result['error'] = true;
	      $result['message'] = $e->getMessage();
	      $result['status'] = 500;
	    }
	    $time_end = microtime(true);
	    $this->logger->debug('po :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );

	    return $res->withJson($result);
   }


   public function getTop($req, $res, $arg){	
	
		$q = "SELECT * from T052U where SPRAS = 'E' AND MANDT = '700'";	
		 $time_start = microtime(true);
		try {
	      $ps = $this->staging->query($q);
	      $ps->execute();

	      $data_displayed = 0;
	      while($row = $ps->fetch()) {
	        $result['data'][] = $row;
	        $data_displayed ++;
	      }
	      $result['count'] = $data_displayed;
	    } catch (\Exception $e) {
	      $result['error'] = true;
	      $result['message'] = $e->getMessage();
	      $result['status'] = 500;
	    }
	    $time_end = microtime(true);
	    $this->logger->debug('TOP :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );

	    return $res->withJson($result);
	}

   
   public function getVendor($req, $res, $arg){
   		//	$search_key = array("BUKRS" => "BUKRS" , "LIFNR" => "lb.LIFNR");
   			$search_key = array("LIFNR" => "lb.LIFNR");
   			$where = '';

   			foreach ($arg as $key => $val) {
		      if (array_key_exists($key, $search_key)) {
		        $where .= $search_key[$key]." like '%".strtoupper($val)."%' AND ";
		      }
    		}

    	
		$where = substr($where, 0, -5);
    	$limit = (isset($arg['LIMIT'])) ? $arg['LIMIT'] : 500;
	    $offset = (isset($arg['OFFSET'])) ? $arg['OFFSET'] : 0;
	    $offset_limit = " OFFSET $offset ROWS FETCH NEXT $limit ROWS ONLY";

	    $q = "SELECT distinct lb.LIFNR,lf.NAME1,lb.AKONT,lf.KTOKK,ad.street,ad.STR_SUPPL1,ad.STR_SUPPL2,ad.CITY1,
        (SELECT DISTINCT t0.landx from T005T t0
         WHERE
            t0.land1  = ad.country and 
            t0.spras = 'E'
        ) AS LANDX,
          bk.BANKS ,bk.BANKL,bk.BANKN,bk.BVTYP,bk.KOINH,
          (select ka.BANKA
                from bnka ka 
                where
                ka.bankl = bk.BANKL
          ) as BANKA
          FROM LFB1 lb 
	    	  inner join LFA1_SAP lf on lf.LIFNR = lb.LIFNR
	    	  inner join ADRC_SAP ad on lf.adrnr =  ad.addrnumber
              inner join LFBK bk on bk.lifnr = lf.lifnr
	    	  where $where order by lb.lifnr";

	   	$time_start = microtime(true);
		try {
	      $ps = $this->staging->query($q);
	      $ps->execute();

	      $data_displayed = 0;
	      while($row = $ps->fetch()) {
	        $result['data'][] = $row;
	        $data_displayed ++;
	      }
	      $result['count'] = $data_displayed;
	    } catch (\Exception $e) {
	      $result['error'] = true;
	      $result['message'] = $e->getMessage();
	      $result['status'] = 500;
	    }
	    $time_end = microtime(true);
	    $this->logger->debug('vendor :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );

	    return $res->withJson($result);
   }


   public function getInvoice($req, $res, $arg){

   			$search_key = array("EBELN" => "ko.EBELN","BUKRS" => "ko.BUKRS");
   			$where = "where ko.BSTYP = 'F' AND ko.LOEKZ =' ' AND ";


   			foreach ($arg as $key => $val) {
		      if (array_key_exists($key, $search_key)) {
		        $where .= $search_key[$key]." like '%".strtoupper($val)."%' AND ";
		      }
    		}

    	$where = substr($where, 0, -5);

    	$limit = (isset($arg['LIMIT'])) ? $arg['LIMIT'] : 500;
	    $offset = (isset($arg['OFFSET'])) ? $arg['OFFSET'] : 0;
	    $offset_limit = " OFFSET $offset ROWS FETCH NEXT $limit ROWS ONLY";

	   /* $q = "SELECT ko.EBELN,ko.BSART,ko.BUKRS,po.WERKS,ko.LIFNR,lf.NAME1 from EKKO_SAP ko 
	    	inner join EKPO_SAP po on po.EBELN = ko.EBELN 
	    	inner join LFA1_SAP lf on lf.LIFNR = ko.LIFNR $where";*/

	   $q = "SELECT bs.belnr,ko.EBELN,po.EBELP,ko.BSART,ko.BUKRS,po.WERKS,ko.LIFNR,lf.NAME1,ko.KNUMV,ko.ZTERM,bs.SGTXT  
	   		 from EKKO_SAP ko 
	    	 inner join EKPO_SAP po on po.EBELN = ko.EBELN 
	    	 inner join LFA1_SAP lf on lf.LIFNR = ko.LIFNR
             inner join ZBSEG_ALL_SAP bs on bs.ebeln = po.ebeln and bs.ebelp = po.ebelp $where";



	    $time_start = microtime(true);
	    try {
	      $ps = $this->staging->query($q);
	      $ps->execute();

	      $data_displayed = 0;
	      while($row = $ps->fetch()) {
	        $result['data'][] = $row;
	        $data_displayed ++;
	      }
	      $result['count'] = $data_displayed;
	    } catch (\Exception $e) {
	      $result['error'] = true;
	      $result['message'] = $e->getMessage();
	      $result['status'] = 500;
	    }
	    $time_end = microtime(true);

	    

	    
	    $this->logger->debug('GETpo INV :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );

	    return $res->withJson($result);


   }

   public function getPohist($req, $res, $arg){
   		    $search_key = array("EBELN" => "ko.EBELN");
   			$where = "where ko.BSTYP = 'F' AND ko.LOEKZ =' ' AND be.bwart != ' ' AND be.BEWTP = 'E' AND ";

   			foreach ($arg as $key => $val) {
		      if (array_key_exists($key, $search_key)) {
		        $where .= $search_key[$key]." like '%".strtoupper($val)."%' ";
		      }
    		}

    		

    	$limit = (isset($arg['LIMIT'])) ? $arg['LIMIT'] : 500;
	    $offset = (isset($arg['OFFSET'])) ? $arg['OFFSET'] : 0;
	    $offset_limit = " OFFSET $offset ROWS FETCH NEXT $limit ROWS ONLY";

	    $q = "SELECT po.EBELN,be.BELNR ,be.BUDAT,po.MATNR,(select MAKTX from MAKT_SAP mak where mak.matnr = po.matnr ) as MAKTX,be.MENGE,po.MEINS,( be.DMBTR * 100 ) as DMBTR ,be.BWART 
            from 
            EKBE be
            inner join ekko_sap ko on be.EBELN = ko.EBELN
            inner join ekpo_sap po on po.EBELN = be.EBELN and 
                                  po.EBELP = be.EBELP
            $where ";

            $time_start = microtime(true);
	    try {
	      $ps = $this->staging->query($q);
	      $ps->execute();

	      $data_displayed = 0;
	      while($row = $ps->fetch()) {
	        $result['data'][] = $row;
	        $data_displayed ++;
	      }
	      $result['count'] = $data_displayed;
	    } catch (\Exception $e) {
	      $result['error'] = true;
	      $result['message'] = $e->getMessage();
	      $result['status'] = 500;
	    }
	    $time_end = microtime(true);

	    
	    $this->logger->debug('GET PO hist:'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );

	    return $res->withJson($result);

   }

   public function getInvhist($req, $res, $arg){	
   		    $search_key = array("EBELN" => "rg.EBELN","GJAHR" => "rg.GJAHR");
   			$where = "WHERE ";

   			foreach ($arg as $key => $val) {
		      if (array_key_exists($key, $search_key)) {
		        $where .= $search_key[$key]." like '%".strtoupper($val)."%' AND ";
		      }
    		}

    		$where = substr($where, 0, -5);

    		$limit = (isset($arg['LIMIT'])) ? $arg['LIMIT'] : 500;
		    $offset = (isset($arg['OFFSET'])) ? $arg['OFFSET'] : 0;
		    $offset_limit = " OFFSET $offset ROWS FETCH NEXT $limit ROWS ONLY";

		    $q = "SELECT rg.BELNR,rg.EBELN, rg.GJAHR,rg.LFBNR,kp.BUDAT,( rg.WRBTR * 100 ) as WRBTR,rg.MWSKZ ,rg.matnr,(SELECT mk.MAKTX FROM MAKT_SAP MK WHERE mk.matnr = rg.matnr) as MAKTX,rg.shkzg,rg.menge,rg.buzei
            FROM 
            RSEG rg 
            inner join  rbkp kp on 
            	rg.BELNR = kp.BELNR and 
            	rg.GJAHR = kp.GJAHR
            $where ";


            $time_start = microtime(true);
	    try {
	      $ps = $this->staging->query($q);
	      $ps->execute();

	      $data_displayed = 0;
	      while($row = $ps->fetch()) {
	        $result['data'][] = $row;
	        $data_displayed ++;
	      }
	      $result['count'] = $data_displayed;
	    } catch (\Exception $e) {
	      $result['error'] = true;
	      $result['message'] = $e->getMessage();
	      $result['status'] = 500;
	    }
	    $time_end = microtime(true);

	    
	    $this->logger->debug('GET INV hist:'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );

	    return $res->withJson($result);


   }

   public function getPlant($req, $res, $arg){
   			$search_key = array("BUKRS" => "WERKS");
   			$where = "WHERE ";

   			foreach ($arg as $key => $val) {
		      if (array_key_exists($key, $search_key)) {
		        $where .= $search_key[$key]." like '".strtoupper($val)."%' AND ";
		      }
    		}	

    		$where = substr($where, 0, -5);


    		$limit = (isset($arg['LIMIT'])) ? $arg['LIMIT'] : 500;
		    $offset = (isset($arg['OFFSET'])) ? $arg['OFFSET'] : 0;
		    $offset_limit = " OFFSET $offset ROWS FETCH NEXT $limit ROWS ONLY";

		    $q = "SELECT WERKS, NAME1 FROM T001W $where";


            $time_start = microtime(true);
	    try {
	      $ps = $this->staging->query($q);
	      $ps->execute();

	      $data_displayed = 0;
	      while($row = $ps->fetch()) {
	        $result['data'][] = $row;
	        $data_displayed ++;
	      }
	      $result['count'] = $data_displayed;
	    } catch (\Exception $e) {
	      $result['error'] = true;
	      $result['message'] = $e->getMessage();
	      $result['status'] = 500;
	    }
	    $time_end = microtime(true);

	    
	    $this->logger->debug('GET Plant :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );

	    return $res->withJson($result);

   }

   public function getInvoicedtlperpo($req, $res, $arg){

   			$search_key = array("EBELN" => "bs.EBELN","GJAHR" => "bs.GJAHR","BUKRS" => "bs.BUKRS");
   			$where = "WHERE  bs.BUZEI = '1' ";

   			foreach ($arg as $key => $val) {
		      if (array_key_exists($key, $search_key)) {
		        $where .= $search_key[$key]." like '%".strtoupper($val)."%' ";
		      }
    		}

    		$limit = (isset($arg['LIMIT'])) ? $arg['LIMIT'] : 500;
		    $offset = (isset($arg['OFFSET'])) ? $arg['OFFSET'] : 0;
		    $offset_limit = " OFFSET $offset ROWS FETCH NEXT $limit ROWS ONLY";

		    $q = "SELECT bs.BELNR ,bs.EBELN,( bs.DMBTR * 100 )  as DPP,  from ZBSEG_ALL_SAP bs $where";

		$time_start = microtime(true);
	    try {
	      $ps = $this->staging->query($q);
	      $ps->execute();

	      $data_displayed = 0;
	      while($row = $ps->fetch()) {
	        $result['data'][] = $row;
	        $data_displayed ++;
	      }
	      $result['count'] = $data_displayed;
	    } catch (\Exception $e) {
	      $result['error'] = true;
	      $result['message'] = $e->getMessage();
	      $result['status'] = 500;
	    }
	    $time_end = microtime(true);

	    
	    $this->logger->debug('GET inv detail :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );

	    return $res->withJson($result);

   }


   public function getBankmaster($req, $res, $arg){

   		 	$search_key = array("BUKRS" => "t.BUKRS");
   			$where = "WHERE ";

   			foreach ($arg as $key => $val) {
		      if (array_key_exists($key, $search_key)) {
		        $where .= $search_key[$key]." like '".strtoupper($val)."%' AND ";
		      }
    		}	

    		$where = substr($where, 0, -5);


    		$limit = (isset($arg['LIMIT'])) ? $arg['LIMIT'] : 500;
		    $offset = (isset($arg['OFFSET'])) ? $arg['OFFSET'] : 0;
		    $offset_limit = " OFFSET $offset ROWS FETCH NEXT $limit ROWS ONLY";

		    $q = "SELECT t.BUKRS,s.BUTXT, t.TEXT1 from T012T_SAP t 
      			 inner join T001_SAP s on t.BUKRS = s.BUKRS  $where";


            $time_start = microtime(true);
	    try {
	      $ps = $this->staging->query($q);
	      $ps->execute();

	      $data_displayed = 0;
	      while($row = $ps->fetch()) {
	        $result['data'][] = $row;
	        $data_displayed ++;
	      }
	      $result['count'] = $data_displayed;
	    } catch (\Exception $e) {
	      $result['error'] = true;
	      $result['message'] = $e->getMessage();
	      $result['status'] = 500;
	    }
	    $time_end = microtime(true);

	    
	    $this->logger->debug('GET bank :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );

	    return $res->withJson($result);

   }

   public function getPaymentVoucher($req, $res, $arg){

   		$search_key = array("BELNR1" => "zhead.BELNR1" ,"BUKRS" => "zbs.BUKRS" ,"GJAHR" =>"zbs.GJAHR");
   		$where = "WHERE zbs.buzei = 001 AND 
       			  zbs.augbl is not null AND ";

   		foreach ($arg as $key => $val) {
		      if (array_key_exists($key, $search_key)) {
		        $where .= $search_key[$key]." = '".strtoupper($val)."' AND ";
		      }
    		}	

    		$where = substr($where, 0, -5);



    		$limit = (isset($arg['LIMIT'])) ? $arg['LIMIT'] : 500;
		    $offset = (isset($arg['OFFSET'])) ? $arg['OFFSET'] : 0;
		    $offset_limit = " OFFSET $offset ROWS FETCH NEXT $limit ROWS ONLY";


		/*$q = "SELECT zhead.belnr1,zhead.bukrs,zbs.augbl,zbs.augcp,
    (SELECT (zbs2.dmbtr * 100) as dmbtr from ZBSEG_ALL@SAPTST_LINK zbs2 where zbs2.belnr = zbs.augbl and zbs2.bukrs = zbs.bukrs  and zbs2.KTOSL = 'ZAH') as DMBTR ,
    (SELECT bk.budat from bkpf_sap bk where bk.belnr = zbs.augbl and bk.bukrs = zbs.bukrs) as BUDAT,
    (SELECT bk.gjahr from bkpf_sap bk where bk.belnr = zbs.augbl and bk.bukrs = zbs.bukrs) as GJAHR
    from zff_header zhead 
       join ZBSEG_ALL@SAPTST_LINK zbs
       on zhead.belnr1 = zbs.belnr $where"; */

       $q = "SELECT zhead.belnr1,zhead.bukrs,zbs.augbl,zbs.augcp,(zbs.dmbtr * 100 ) as dmbtr,
    (SELECT bk.budat from bkpf_sap bk where bk.belnr = zbs.augbl and bk.bukrs = zbs.bukrs) as BUDAT,
    (SELECT bk.gjahr from bkpf_sap bk where bk.belnr = zbs.augbl and bk.bukrs = zbs.bukrs) as GJAHR
    from zff_header zhead 
       join ZBSEG_ALL_SAP zbs
       on zhead.belnr1 = zbs.belnr $where";


        $time_start = microtime(true);
	    try {
	      $ps = $this->staging->query($q);
	      $ps->execute();

	      $data_displayed = 0;
	      while($row = $ps->fetch()) {
	        $result['data'][] = $row;
	        $data_displayed ++;
	      }
	      $result['count'] = $data_displayed;
	    } catch (\Exception $e) {
	      $result['error'] = true;
	      $result['message'] = $e->getMessage();
	      $result['status'] = 500;
	    }
	    $time_end = microtime(true);

	    
	    $this->logger->debug('GET PAYMENT VOUCHER :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );

	    return $res->withJson($result);

   }


   public function getPpn($req, $res, $arg){

   		$search_key = array("BELNR1" => "zhead.BELNR1" ,"BUKRS" => "zbs.BUKRS" );
   		$where = " WHERE zbs.hkont = '1105020101' AND ";

   		foreach ($arg as $key => $val) {
		      if (array_key_exists($key, $search_key)) {
		        $where .= $search_key[$key]." = '".strtoupper($val)."' AND ";
		      }
    		}	

    		$where = substr($where, 0, -5);



    		$limit = (isset($arg['LIMIT'])) ? $arg['LIMIT'] : 500;
		    $offset = (isset($arg['OFFSET'])) ? $arg['OFFSET'] : 0;
		    $offset_limit = " OFFSET $offset ROWS FETCH NEXT $limit ROWS ONLY";


	$q = "SELECT zhead.belnr1 ,(zbs.dmbtr * 100) as dmbtr ,zhead.bukrs , zbs.gjahr
		    from zff_header zhead
		    join ZBSEG_ALL_SAP zbs
		    on zhead.belnr1 = zbs.belnr and 
		       zhead.bukrs = zbs.bukrs  $where";


        $time_start = microtime(true);
	    try {
	      $ps = $this->staging->query($q);
	      $ps->execute();

	      $data_displayed = 0;
	      while($row = $ps->fetch()) {
	        $result['data'][] = $row;
	        $data_displayed ++;
	      }
	      $result['count'] = $data_displayed;
	    } catch (\Exception $e) {
	      $result['error'] = true;
	      $result['message'] = $e->getMessage();
	      $result['status'] = 500;
	    }
	    $time_end = microtime(true);

	    
	    $this->logger->debug('GET PPN :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );

	    return $res->withJson($result);

   }


   public function getPph($req, $res, $arg){

   		$search_key = array("BELNR1" => "zhead.BELNR1" ,"BUKRS" => "zbs.BUKRS" );
   		/*$where = "WHERE zbs.buzei = 1 AND 
       			  zbs.augbl is not null AND "; */

       	$where = "WHERE zbs.augbl is not null AND ";

   		foreach ($arg as $key => $val) {
		      if (array_key_exists($key, $search_key)) {
		        $where .= $search_key[$key]." = '".strtoupper($val)."' AND ";
		      }
    		}	

    		$where = substr($where, 0, -5);



    		$limit = (isset($arg['LIMIT'])) ? $arg['LIMIT'] : 500;
		    $offset = (isset($arg['OFFSET'])) ? $arg['OFFSET'] : 0;
		    $offset_limit = " OFFSET $offset ROWS FETCH NEXT $limit ROWS ONLY";


	$q = "SELECT zhead.belnr1,zhead.bukrs,zbs.augbl,zbs.augcp,
    (SELECT (zbs2.dmbtr * 100) as dmbtr from ZBSEG_ALL_SAP zbs2 where zbs2.belnr = zbs.augbl and zbs2.bukrs = zbs.bukrs  and zbs2.KTOSL = 'WIT') as DMBTR ,
    (SELECT bk.budat from bkpf_sap bk where bk.belnr = zbs.augbl and bk.bukrs = zbs.bukrs) as BUDAT,
    (SELECT bk.gjahr from bkpf_sap bk where bk.belnr = zbs.augbl and bk.bukrs = zbs.bukrs) as GJAHR
    from zff_header zhead 
       join ZBSEG_ALL_SAP zbs
       on zhead.belnr1 = zbs.belnr $where";


        $time_start = microtime(true);
	    try {
	      $ps = $this->staging->query($q);
	      $ps->execute();

	      $data_displayed = 0;
	      while($row = $ps->fetch()) {
	        $result['data'][] = $row;
	        $data_displayed ++;
	      }
	      $result['count'] = $data_displayed;
	    } catch (\Exception $e) {
	      $result['error'] = true;
	      $result['message'] = $e->getMessage();
	      $result['status'] = 500;
	    }
	    $time_end = microtime(true);

	    
	    $this->logger->debug('GET PPH :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );

	    return $res->withJson($result);

   }

   public function getDpp($req, $res, $arg){

   		$search_key = array("BUKRS" => "zff.BUKRS" , "GJAHR" => "zff.GJAHR","BELNR1" => "zff.BELNR1");
   		$where = "WHERE ";
   		foreach ($arg as $key => $val) {

		      if (array_key_exists($key, $search_key)) {
		        $where .= $search_key[$key]." = '".strtoupper($val)."' AND ";
		      }
    		}	

    		$where = substr($where, 0, -5);


    		$limit = (isset($arg['LIMIT'])) ? $arg['LIMIT'] : 500;
		    $offset = (isset($arg['OFFSET'])) ? $arg['OFFSET'] : 0;
		    $offset_limit = " OFFSET $offset ROWS FETCH NEXT $limit ROWS ONLY";


		    $q = "select zff.BELNR1 ,zff.gjahr,zff.EBELN,
		            CASE 
		                WHEN zff.EBELN  = ' '
		                then
		                    (select sum(zs.dmbtr * 100) from ZBSEG_ALL_SAP zs where zs.belnr = zff.BELNR1  and zs.gjahr = zff.gjahr and zs.bukrs = zff.bukrs and zs.KOSTL != ' ')
		                when zff.EBELN  != ' '
		                then
		                    (select sum(zs.dmbtr * 100) from ZBSEG_ALL_SAP zs where zs.belnr = zff.BELNR1  and zs.gjahr = zff.gjahr and zs.bukrs = zff.bukrs and zs.HKONT in ('2103030101','2104030101'))
		            END as DMBTR
		                
		            from zff_header zff $where";

		$time_start = microtime(true);
	    try {
	      $ps = $this->staging->query($q);
	      $ps->execute();

	      $data_displayed = 0;
	      while($row = $ps->fetch()) {
	        $result['data'][] = $row;
	        $data_displayed ++;
	      }
	      $result['count'] = $data_displayed;
	    } catch (\Exception $e) {
	      $result['error'] = true;
	      $result['message'] = $e->getMessage();
	      $result['status'] = 500;
	    }
	    $time_end = microtime(true);

	    
	    $this->logger->debug('GET DPP :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );

	    return $res->withJson($result);

   }


   public function getPaymentHist($req, $res, $arg){
   	$search_key = array("EBELN" => "zff.EBELN" ,"BUKRS" => "zff.bukrs");
   	$where = "WHERE zff.belnr1 is not null AND ";

   			foreach ($arg as $key => $val) {

		      if (array_key_exists($key, $search_key)) {
		        $where .= $search_key[$key]." = '".strtoupper($val)."' AND ";
		      }
    		}	

    		$where = substr($where, 0, -5);


    		$limit = (isset($arg['LIMIT'])) ? $arg['LIMIT'] : 500;
		    $offset = (isset($arg['OFFSET'])) ? $arg['OFFSET'] : 0;
		    $offset_limit = " OFFSET $offset ROWS FETCH NEXT $limit ROWS ONLY";


		   $q = "select zff.ebeln ,zff.belnr1,zff.gjahr,zff.bukrs,
    				(select (bs.dmbtr * 100 ) from ZBSEG_ALL_SAP bs 
        		where bs.bukrs = zff.bukrs and bs.gjahr = zff.gjahr and bs.buzei = 1 and bs.belnr = zff.BELNR1 and bs.augbl != ' ') as dmbtr,
    			(select bs.augbl  from ZBSEG_ALL_SAP bs 
		        where bs.bukrs = zff.bukrs and bs.gjahr = zff.gjahr and bs.buzei = 1 and bs.belnr = zff.BELNR1 and bs.augbl != ' ') as augbl,
		        (select bs.augdt  from ZBSEG_ALL_SAP bs 
		        where bs.bukrs = zff.bukrs and bs.gjahr = zff.gjahr and bs.buzei = 1 and bs.belnr = zff.BELNR1 and bs.augbl != ' ') as augdt
		        from zff_header zff  $where ";

	 	$time_start = microtime(true);
	    try {
	      $ps = $this->staging->query($q);
	      $ps->execute();

	      $data_displayed = 0;
	      while($row = $ps->fetch()) {
	        $result['data'][] = $row;
	        $data_displayed ++;
	      }
	      $result['count'] = $data_displayed;
	    } catch (\Exception $e) {
	      $result['error'] = true;
	      $result['message'] = $e->getMessage();
	      $result['status'] = 500;
	    }
	    $time_end = microtime(true);

	    
	    $this->logger->debug('GET PAYMENT HISTORY :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );

	    return $res->withJson($result);

   }


}



?>