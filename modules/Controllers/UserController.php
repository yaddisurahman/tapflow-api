<?php 

namespace Modules\Controllers;

class UserController {

  public function index($req, $res)
  {
    echo $user = $req->getParam('username');
    $res->withHeader('Content-type', 'application/json');
    return $res->withJson(['error' => true, 'message' => 'No direct access allowed']);
  }

  public function validateUser($req, $res, $param)
  {
    $res->withHeader('Content-type', 'application/json');
    if(!isset($param['username']) || !isset($param['password'])) {
      $res->withJson(['valid' => false, 'message' => 'Username and Password required']);
      $res->withHeader('status',400);
      $res->withStatus(400);
      return $res;
    }

    $username = $param['username'];
    $password = $param['password'];
    return $this->ldap($req, $res, $username, $password);
  }

  public function ldap($req, $res, $username, $password) {
    $res->withHeader('Content-type', 'application/json');
    $conn = ldap_connect('ldap.tap-agri.com', 389);
    $bind = @ldap_bind($conn, "tap\\$username", "$password");
    $err_message = ldap_error($conn) ;

    if($bind) {
      $res->withJson(['valid' => true]);
    } else {
      $res->withJson(['valid' => false, 'message' => $err_message]);
    }

    return $res;
  }
}
