<?php

namespace Modules\Controllers;

class LdapController extends Controller
{

  private $ldap;
  function __construct($container)
  {
    $this->ldap = $container->get('settings')['ldap'];
  }

  public function test($req, $res)
  {
    $loggerSettings = $this->ldap;
    return $res->withJson($loggerSettings);
  }

  public function search($req, $res, $arg)
  {
    $ldap = $this->ldap;

    try {
      $conn = ldap_connect($ldap['ldaphost'], $ldap['ldapport']);
      try {
        $bind = @ldap_bind($conn, "tap\\".$ldap['admin_username'], $ldap['admin_password']);
        try {
          $search = ldap_search(
                        $conn, 
                        "OU=B.Triputra Agro Persada, DC=tap, DC=corp", 
                        "(mail=*".$arg['email']."*)", array("mail", "sn", "cn", "sama", "name", "title"), 0
                        , (isset($arg['limit'])) ? $arg['limit'] : $ldap["result_filter"]
                      );
          $entry_count = ldap_count_entries($conn, $search);
          $entries     = ldap_get_entries($conn, $search);

          $data = [];
          foreach ($entries as $key) {
            $entry['name'] = $key['name'][0];
            $entry['email'] = $key['mail'][0];
            $entry['dn'] = $key['dn'];
            array_push($data, $entry);
          }
          unset($data[0]);

        } catch (Exception $e) {
          $res->withJson(ldap_error($conn));
        } finally {
          ldap_close($conn);
          return $res->withJson(['count' => $entry_count, 'data' => $data]);
        }
      } catch (Exception $e) {
        $res->withJson(ldap_error($conn));
      }
    } catch (Exception $e) {
      $res->withJson(ldap_error($conn));
      return $res;
    }
  }

}
