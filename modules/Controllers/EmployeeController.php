<?php 

namespace Modules\Controllers;

class EmployeeController extends Controller
{

  /**
   * Redirect ke search
   */
  public function index($req, $res, $arg)
  {
    return $this->search($req, $res, $arg);
  }

  public function search($req, $res, $arg)
  {
    $column_name = $this->getColumnName('TM_EMPLOYEE_PERSONALIA', $this->tapflow);
    $employee_search = array();
    $where = '';
    foreach ($column_name as $key) {
      $employee_search[$key] = 'UPPER('.$key.')';
    }

    foreach ($arg as $key => $val) {
      if (array_key_exists($key, $employee_search)) {
        $where .= $this->query_helper->generateFilters($key, $val, $employee_search[$key]);
      }
      if($key == 'WERKS') {
        $where .= $this->query_helper->generateFilters('WERKS', $val, 'SUBSTR(NIK_SAP,4,4)');
      }
    }

    $limit = (isset($arg['LIMIT'])) ? $arg['LIMIT'] : 1000;
    $offset = (isset($arg['OFFSET'])) ? $arg['OFFSET'] : 0;

    foreach (array_keys($arg) as $key) {
      if($key == 'NIK') {
        $where .= '('.$this->query_helper->generateFilters('NIK_SAP', $arg[$key], 'NIK_SAP');
        $where = substr($where, 0, -5);
        $where .= ' OR '.$this->query_helper->generateFilters('NIK_NASIONAL', $arg[$key], 'NIK_NASIONAL');
        $where = substr($where, 0, -5).') AND ';
      }
    }

    $where = substr($where, 0, -5);
    if ($where != '') {
      $where = 'AND '.$where;
    }

    $offset_limit = "OFFSET $offset ROWS FETCH NEXT $limit ROWS ONLY";

    $fields = ['NIK'=>'INS.NIK_SAP','ADDRESS'=>'INS.ADDRESS','AFD_CODE'=>'INS.AFD_CODE','ASTEK_TYPE'=>'INS.ASTEK_TYPE','BANK_ACCOUNT_NUMBER'=>'INS.BANK_ACCOUNT_NUMBER','BANK_COUNTRY_KEY'=>'INS.BANK_COUNTRY_KEY','BANK_KEYS'=>'INS.BANK_KEYS','BOND_EXPIRE_DATE'=>'INS.BOND_EXPIRE_DATE','BPJS_KESEHATAN'=>'INS.BPJS_KESEHATAN','BPJS_KETENAGAKERJAAN'=>'INS.BPJS_KETENAGAKERJAAN','COMP_CODE'=>'INS.COMP_CODE','CONDUCTOR_NAME'=>'INS.CONDUCTOR_NAME','CONTRACT_TO'=>'INS.CONTRACT_TO','CURRENCY'=>'INS.CURRENCY','CUSTOMER'=>'INS.CUSTOMER','DEPARTEMEN'=>'INS.DEPARTEMEN','DESCRIPTION'=>'INS.DESCRIPTION','DOB'=>'INS.DOB','DOMISILI_ID'=>'INS.DOMISILI_ID','EMAIL'=>'INS.EMAIL','EMP_STAT'=>'INS.EMP_STAT','EMP_STAT1'=>'INS.EMP_STAT1','EMPLOYEE_NAME'=>'INS.EMPLOYEE_NAME','EMPLOYEE_STATUS_INDICATOR'=>'INS.EMPLOYEE_STATUS_INDICATOR','END_VALID'=>'INS.END_VALID','JOIN_DATE'=>'INS.ENTRY_DATE','EPF_NUMBER'=>'INS.EPF_NUMBER','EPF_PERCENTAGE'=>'INS.EPF_PERCENTAGE','EPF_TYPE'=>'INS.EPF_TYPE','EST_CODE'=>'INS.EST_CODE','EXPIRE_DATE_CONTRACT'=>'INS.EXPIRE_DATE_CONTRACT','FINGER_CODE'=>'INS.FINGER_CODE','GANGCODE'=>'INS.GANGCODE','GOLONGAN'=>'INS.GOLONGAN','HOME_PHONE'=>'INS.HOME_PHONE','IC_NUMBER_NEW'=>'INS.IC_NUMBER_NEW','IC_NUMBER_OLD'=>'INS.IC_NUMBER_OLD','IC_SARAWAK_REGION'=>'INS.IC_SARAWAK_REGION','IDENTIFICATION'=>'INS.IDENTIFICATION','INDCTR'=>'INS.INDCTR','INSENTIF'=>'INS.INSENTIF','JOB_CODE'=>'INS.JOB_CODE','JOB_TYPE'=>'INS.JOB_TYPE','LEVEL_EMPLOYEE'=>'INS.LEVEL_EMPLOYEE','LICENSE_ID'=>'INS.LICENSE_ID','MOBILE_PHONE'=>'INS.MOBILE_PHONE','NIK_NASIONAL'=>'INS.NIK_NASIONAL','NIK_OLD'=>'INS.NIK_OLD','NO_KK'=>'INS.NO_KK','NO_KTP'=>'INS.NO_KTP','NPWP'=>'INS.NPWP','ORGANIZATION_CODE'=>'INS.ORGANIZATION_CODE','PASSPORT_EXPIRE_DATE'=>'INS.PASSPORT_EXPIRE_DATE','PASSPORT_NUMBER'=>'INS.PASSPORT_NUMBER','PAYMENT_TYPE'=>'INS.PAYMENT_TYPE','PERIOD_PROBATION'=>'INS.PERIOD_PROBATION','PHASE'=>'INS.PHASE','PHONE'=>'INS.PHONE','POB'=>'INS.POB','POSITION'=>'INS.POSITION','PROF_NAME'=>'INS.PROF_NAME','RACE'=>'INS.RACE','RELIGION'=>'INS.RELIGION','RES_DATE'=>'INS.RES_DATE','RESIDENT_FLAG'=>'INS.RESIDENT_FLAG','RICE_PORTION_OPTIONS'=>'INS.RICE_PORTION_OPTIONS','SALARY'=>'INS.SALARY','SALARY_TYPE'=>'INS.SALARY_TYPE','SEX'=>'INS.SEX','SOCSO_NUMBER'=>'INS.SOCSO_NUMBER','SPK_SK'=>'INS.SPK_SK','START_VALID'=>'INS.START_VALID','STATUS'=>'INS.STATUS','TIME_ID'=>'INS.TIME_ID','WORK_PERMIT_EXPIRE_DATE'=>'INS.WORK_PERMIT_EXPIRE_DATE','AFD_NAME'=>'AFD.AFD_NAME','WERKS'=>'AREA.AREA_CODE','COMP_NAME'=>'COMP.COMP_NAME','EST_NAME'=>'AREA.PAYROLL'];
    $selected_fields = $this->query_helper->fieldAlias($fields);

    $sql = "WITH EMPLOYEE_INSTANCE AS (
              SELECT * FROM TM_EMPLOYEE_PERSONALIA
              WHERE TRUNC(TO_DATE('".date('Y-m-d')."', 'YYYY-MM-DD'), 'MM') BETWEEN START_VALID AND
                CASE WHEN EXTRACT (YEAR FROM RES_DATE) != 9999
                  THEN RES_DATE
                  ELSE END_VALID
                END $where
            )
            SELECT $selected_fields,DECODE(SUBSTR(AREA.AREA_CODE, 3, 1), '4', 'MILL', 'ESTATE') ESTATE_MILL,NULL SPV_NIK,'SAP' SOURCE
            FROM EMPLOYEE_INSTANCE INS
            JOIN TM_COMP@DEVDW_LINK COMP ON INS.COMP_CODE = COMP.COMP_CODE
            JOIN TM_AREA_CODE@DEVDW_LINK AREA ON AREA.AREA_CODE = SUBSTR(INS.NIK_SAP,4,4)
            LEFT JOIN TM_AFD@DEVDW_LINK AFD ON INS.COMP_CODE = AFD.COMP_CODE AND INS.EST_CODE = AFD.EST_CODE AND INS.AFD_CODE = AFD.AFD_CODE
            ";

    $time_start = microtime(true);
    try {
      $ps = $this->tapflow->query($sql);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'][] = $this->query_helper->nullerRow($row, 'N/A');
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }
    $res->withHeader('Content-type', 'application/json');

    $time_end = microtime(true);
    $this->logger->debug('employee/search :'. str_replace('  ', '', $sql), array('benchmark' => $time_end - $time_start) );

    return $res->withJson($result);
  }

  public function getEmployee($req, $res, $arg)
  {
    if(!$arg['NIK']) {
      $res->withHeader('Content-type', 'application/json');
      return $res->withJson(['error' => true, 'status' => 500, 'message' => 'Parameter not satisfied']);
    }

    $fields = ['NIK'=>'INS.NIK_SAP','ADDRESS'=>'INS.ADDRESS','AFD_CODE'=>'INS.AFD_CODE','ASTEK_TYPE'=>'INS.ASTEK_TYPE','BANK_ACCOUNT_NUMBER'=>'INS.BANK_ACCOUNT_NUMBER','BANK_COUNTRY_KEY'=>'INS.BANK_COUNTRY_KEY','BANK_KEYS'=>'INS.BANK_KEYS','BOND_EXPIRE_DATE'=>'INS.BOND_EXPIRE_DATE','BPJS_KESEHATAN'=>'INS.BPJS_KESEHATAN','BPJS_KETENAGAKERJAAN'=>'INS.BPJS_KETENAGAKERJAAN','COMP_CODE'=>'INS.COMP_CODE','CONDUCTOR_NAME'=>'INS.CONDUCTOR_NAME','CONTRACT_TO'=>'INS.CONTRACT_TO','CURRENCY'=>'INS.CURRENCY','CUSTOMER'=>'INS.CUSTOMER','DEPARTEMEN'=>'INS.DEPARTEMEN','DESCRIPTION'=>'INS.DESCRIPTION','DOB'=>'INS.DOB','DOMISILI_ID'=>'INS.DOMISILI_ID','EMAIL'=>'INS.EMAIL','EMP_STAT'=>'INS.EMP_STAT','EMP_STAT1'=>'INS.EMP_STAT1','EMPLOYEE_NAME'=>'INS.EMPLOYEE_NAME','EMPLOYEE_STATUS_INDICATOR'=>'INS.EMPLOYEE_STATUS_INDICATOR','END_VALID'=>'INS.END_VALID','JOIN_DATE'=>'INS.ENTRY_DATE','EPF_NUMBER'=>'INS.EPF_NUMBER','EPF_PERCENTAGE'=>'INS.EPF_PERCENTAGE','EPF_TYPE'=>'INS.EPF_TYPE','EST_CODE'=>'INS.EST_CODE','EXPIRE_DATE_CONTRACT'=>'INS.EXPIRE_DATE_CONTRACT','FINGER_CODE'=>'INS.FINGER_CODE','GANGCODE'=>'INS.GANGCODE','GOLONGAN'=>'INS.GOLONGAN','HOME_PHONE'=>'INS.HOME_PHONE','IC_NUMBER_NEW'=>'INS.IC_NUMBER_NEW','IC_NUMBER_OLD'=>'INS.IC_NUMBER_OLD','IC_SARAWAK_REGION'=>'INS.IC_SARAWAK_REGION','IDENTIFICATION'=>'INS.IDENTIFICATION','INDCTR'=>'INS.INDCTR','INSENTIF'=>'INS.INSENTIF','JOB_CODE'=>'INS.JOB_CODE','JOB_TYPE'=>'INS.JOB_TYPE','LEVEL_EMPLOYEE'=>'INS.LEVEL_EMPLOYEE','LICENSE_ID'=>'INS.LICENSE_ID','MOBILE_PHONE'=>'INS.MOBILE_PHONE','NIK_NASIONAL'=>'INS.NIK_NASIONAL','NIK_OLD'=>'INS.NIK_OLD','NO_KK'=>'INS.NO_KK','NO_KTP'=>'INS.NO_KTP','NPWP'=>'INS.NPWP','ORGANIZATION_CODE'=>'INS.ORGANIZATION_CODE','PASSPORT_EXPIRE_DATE'=>'INS.PASSPORT_EXPIRE_DATE','PASSPORT_NUMBER'=>'INS.PASSPORT_NUMBER','PAYMENT_TYPE'=>'INS.PAYMENT_TYPE','PERIOD_PROBATION'=>'INS.PERIOD_PROBATION','PHASE'=>'INS.PHASE','PHONE'=>'INS.PHONE','POB'=>'INS.POB','POSITION'=>'INS.POSITION','PROF_NAME'=>'INS.PROF_NAME','RACE'=>'INS.RACE','RELIGION'=>'INS.RELIGION','RES_DATE'=>'INS.RES_DATE','RESIDENT_FLAG'=>'INS.RESIDENT_FLAG','RICE_PORTION_OPTIONS'=>'INS.RICE_PORTION_OPTIONS','SALARY'=>'INS.SALARY','SALARY_TYPE'=>'INS.SALARY_TYPE','SEX'=>'INS.SEX','SOCSO_NUMBER'=>'INS.SOCSO_NUMBER','SPK_SK'=>'INS.SPK_SK','START_VALID'=>'INS.START_VALID','STATUS'=>'INS.STATUS','TIME_ID'=>'INS.TIME_ID','WORK_PERMIT_EXPIRE_DATE'=>'INS.WORK_PERMIT_EXPIRE_DATE','AFD_NAME'=>'AFD.AFD_NAME','WERKS'=>'AREA.AREA_CODE','COMP_NAME'=>'COMP.COMP_NAME','EST_NAME'=>'AREA.PAYROLL'];
    $selected_fields = $this->query_helper->fieldAlias($fields);

    $sql = "WITH EMPLOYEE_INSTANCE AS (
              SELECT * FROM TM_EMPLOYEE_PERSONALIA
              WHERE TRUNC(TO_DATE('".date('Y-m-d')."', 'YYYY-MM-DD'), 'MM') BETWEEN START_VALID AND
                CASE WHEN EXTRACT (YEAR FROM RES_DATE) != 9999
                  THEN RES_DATE
                  ELSE END_VALID
                END AND (NIK_SAP = '".$arg['NIK']."' OR NIK_NASIONAL = '".$arg['NIK']."')
            )
            SELECT $selected_fields,DECODE(SUBSTR(AREA.AREA_CODE, 3, 1), '4', 'MILL', 'ESTATE') ESTATE_MILL,NULL SPV_NIK,'SAP' SOURCE
            FROM EMPLOYEE_INSTANCE INS
            JOIN TM_COMP@DEVDW_LINK COMP ON INS.COMP_CODE = COMP.COMP_CODE
            JOIN TM_AREA_CODE@DEVDW_LINK AREA ON AREA.AREA_CODE = SUBSTR(INS.NIK_SAP,4,4)
            LEFT JOIN TM_AFD@DEVDW_LINK AFD ON INS.COMP_CODE = AFD.COMP_CODE AND INS.EST_CODE = AFD.EST_CODE AND INS.AFD_CODE = AFD.AFD_CODE
            ";
    
    try {
      $ps = $this->tapflow->query($sql);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'] = $this->query_helper->nullerRow($row, 'N/A');
        $result['data']['CUTI'] = $this->sisaCuti($arg['NIK']);
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }
    $res->withHeader('Content-type', 'application/json');
    $this->logger->debug('getEmployee :'. str_replace('  ', '', $sql) );

    return $res->withJson($result);
  }

  public function staff($req, $res, $arg)
  {
    $hris_search = [
      "NIK" => "UPPER(EMPLOYEE_NIK)", "EMPLOYEE_NAME" => "UPPER(EMPLOYEE_FULLNAME)", "JOB_TYPE" => "UPPER(EMPLOYEE_GRADE)", "COMP_CODE" => "UPPER(EMPLOYEE_COMPANYCODE)", "SPV_NIK" => "UPPER(EMPLOYEE_SPVNIK)", "JOB_CODE" => "UPPER(EMPLOYEE_DEPARTMENT)", "START_VALID" => "UPPER(EMPLOYEE_JOINDATE)", "RES_DATE" => "UPPER(EMPLOYEE_RESIGNDATE)", "DOB" => "UPPER(EMPLOYEE_BIRTHDAY)", "EMAIL" => "UPPER(EMPLOYEE_EMAIL)", "SEX" => "UPPER(DECODE(EMPLOYEE_GENDER, 'M', 'MALE', 'F', 'FEMALE', EMPLOYEE_GENDER))", "RELIGION" => "UPPER(DECODE(EMPLOYEE_RELIGION, 'MOESLEM', 'ISLAM', 'CATHOLIC', 'KATOLIK', 'CHRISTIAN', 'PROTESTAN', NULL, 'N/A', '(NONE)', 'N/A', EMPLOYEE_RELIGION))", "JOIN_DATE" => "UPPER(EMPLOYEE_JOINDATE)"
    ];
    $where_hris = '';
    foreach ($arg as $key => $val) {
      if (array_key_exists($key, $hris_search)) {
        $where_hris .= $this->query_helper->generateFilters($key, $val, $hris_search[$key]);
      }
    }
    $where_hris = substr($where_hris, 0, -5);

    if ($where_hris != '') {
      $where_hris = 'WHERE '.$where_hris;
    }

    $sql = "SELECT NULL AS WERKS, NULL AS PROF_NAME, NULL AS COMP_CODE, NULL AS EST_CODE, NULL AS AFD_CODE, NULL AS DEPARTEMEN, EMPLOYEE_NIK AS NIK, EMPLOYEE_FULLNAME AS EMPLOYEE_NAME, EMPLOYEE_JOINDATE AS START_VALID, NULL AS END_VALID, NULL AS ENTRY_DATE, EMPLOYEE_RESIGNDATE AS RES_DATE, EMPLOYEE_DEPARTMENT AS JOB_CODE, EMPLOYEE_GRADE AS JOB_TYPE, NULL AS EMP_STAT, NULL AS EMP_STAT1, NULL AS ADDRESS, NULL AS POB, EMPLOYEE_BIRTHDAY AS DOB, DECODE(EMPLOYEE_GENDER, 'M', 'MALE', 'F', 'FEMALE', EMPLOYEE_GENDER) AS SEX, DECODE(EMPLOYEE_RELIGION, 'MOESLEM', 'ISLAM', 'CATHOLIC', 'KATOLIK', 'CHRISTIAN', 'PROTESTAN', NULL, 'N/A', '(NONE)', 'N/A', EMPLOYEE_RELIGION) AS RELIGION, NULL AS STATUS, NULL AS GANGCODE, EMPLOYEE_SPVNIK AS SPV_NIK, NULL AS NO_KTP, NULL AS NPWP, NULL AS FINGER_CODE, NULL AS GOLONGAN, EMPLOYEE_EMAIL AS EMAIL, NULL AS PHONE, NULL AS TIME_ID, NULL AS PHASE, NULL AS CONDUCTOR_NAME, NULL AS CUSTOMER, NULL AS RESIDENT_FLAG, NULL AS IDENTIFICATION, NULL AS HOME_PHONE, NULL AS MOBILE_PHONE, NULL AS SALARY_TYPE, NULL AS POSITION, NULL AS LEVEL_EMPLOYEE, NULL AS CURRENCY, NULL AS SALARY, NULL AS INSENTIF, NULL AS RICE_PORTION_OPTIONS, NULL AS ORGANIZATION_CODE, NULL AS ASTEK_TYPE, NULL AS IC_NUMBER_OLD, NULL AS IC_NUMBER_NEW, NULL AS EPF_NUMBER, NULL AS SOCSO_NUMBER, NULL AS RACE, NULL AS PAYMENT_TYPE, NULL AS IC_SARAWAK_REGION, NULL AS PASSPORT_NUMBER, NULL AS PASSPORT_EXPIRE_DATE, NULL AS WORK_PERMIT_EXPIRE_DATE, NULL AS BOND_EXPIRE_DATE, NULL AS EPF_TYPE, NULL AS EPF_PERCENTAGE, NULL AS BANK_COUNTRY_KEY, NULL AS BANK_KEYS, NULL AS BANK_ACCOUNT_NUMBER, NULL AS DESCRIPTION, NULL AS EMPLOYEE_STATUS_INDICATOR, NULL AS NIK_OLD, NULL AS EXPIRE_DATE_CONTRACT, NULL AS INDCTR, NULL AS PERIOD_PROBATION, NULL AS CONTRACT_TO, NULL AS SPK_SK, NULL AS NO_KK, NULL AS BPJS_KETENAGAKERJAAN, NULL AS BPJS_KESEHATAN, NULL AS DOMISILI_ID, NULL AS LICENSE_ID, NULL AS NIK_NASIONAL, EMPLOYEE_COMPANYCODE AS COMP_CODE, NULL AS COMP_NAME, NULL AS EST_CODE, NULL AS EST_NAME, NULL AS AFD_CODE, NULL AS AFD_NAME, EMPLOYEE_JOINDATE AS JOIN_DATE, 'HRIS' AS DATA_SOURCE, NULL AS ESTATE_MILL
      FROM TM_EMPLOYEE_HRIS $where_hris";
      $this->logger->debug('staff :'. str_replace('  ', '', $sql) );

    // echo $sql; 
    // die();
    try {
      $ps = $this->tapdw->query($sql);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'][] = $this->query_helper->nullerRow($row, 'N/A');
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }
    $res->withHeader('Content-type', 'application/json');
    return $res->withJson($result);

  }

  /**
   * Alias dari index. data selurh karyawan berdasarkan filter
   */
  /**
  public function search($req, $res, $arg)
  {
    $search_key = [
      "COMP_CODE" => "UPPER(CC.COMP_CODE)", "COMP_NAME" => "UPPER(CC.COMP_NAME)", "EST_CODE" => "UPPER(EE.EST_CODE)",
      "EST_NAME" => "UPPER(EE.EST_NAME)", "AFD_CODE" => "UPPER(AA.AFD_CODE)", "AFD_NAME" => "UPPER(AA.AFD_NAME)",
      "JOIN_DATE" => "UPPER(TJ.JOIN_DATE)"
    ];

    $sap_column = $this->getColumnName('TM_EMPLOYEE_SAP');
    $instance_search = array();
    foreach ($sap_column as $key) {
      $instance_search[$key] = 'UPPER('.$key.')';
    }

    $hris_search = [
      "NIK" => "UPPER(EMPLOYEE_NIK)", "EMPLOYEE_NAME" => "UPPER(EMPLOYEE_FULLNAME)", "JOB_TYPE" => "UPPER(EMPLOYEE_GRADE)", "COMP_CODE" => "UPPER(EMPLOYEE_COMPANYCODE)", "SPV_NIK" => "UPPER(EMPLOYEE_SPVNIK)", "JOB_CODE" => "UPPER(EMPLOYEE_DEPARTMENT)", "START_VALID" => "UPPER(EMPLOYEE_JOINDATE)", "RES_DATE" => "UPPER(EMPLOYEE_RESIGNDATE)", "DOB" => "UPPER(EMPLOYEE_BIRTHDAY)", "EMAIL" => "UPPER(EMPLOYEE_EMAIL)", "SEX" => "UPPER(DECODE(EMPLOYEE_GENDER, 'M', 'MALE', 'F', 'FEMALE', EMPLOYEE_GENDER))", "RELIGION" => "UPPER(DECODE(EMPLOYEE_RELIGION, 'MOESLEM', 'ISLAM', 'CATHOLIC', 'KATOLIK', 'CHRISTIAN', 'PROTESTAN', NULL, 'N/A', '(NONE)', 'N/A', EMPLOYEE_RELIGION))", "JOIN_DATE" => "UPPER(EMPLOYEE_JOINDATE)"
    ];

    $limit = (isset($arg['LIMIT'])) ? $arg['LIMIT'] : 1000;
    $offset = (isset($arg['OFFSET'])) ? $arg['OFFSET'] : 0;

    // $hris_column = $this->getColumnName('TM_EMPLOYEE_HRIS');
    // $hris_search = array();
    // foreach ($hris_column as $key) {
    //   $hris_search[$key] = 'UPPER('.$key.')';
    // }

    $where = ''; $where_ins = ''; $where_hris = ''; $union = true;

    foreach ($arg as $key => $val) {
      if (array_key_exists($key, $search_key)) {
        $where .= $this->query_helper->generateFilters($key, $val, $search_key[$key]);
        // $where .= $search_key[$key]." like '%".strtoupper($val)."%' AND ";
      }

      if (array_key_exists($key, $instance_search)) {
        $where_ins .= $this->query_helper->generateFilters($key, $val, $instance_search[$key]);
        // $where_ins .= $instance_search[$key]." like '%".strtoupper($val)."%' AND ";
      }

      if(!array_key_exists($key, $hris_search)) {
        $key . " is note exist on HRIS <br>";
        $union = false;        
      }

      if ($union) {
        if (array_key_exists($key, $hris_search)) {
          $where_hris .= $this->query_helper->generateFilters($key, $val, $hris_search[$key]);
          // $where_hris .= $hris_search[$key]." like '%".strtoupper($val)."%' AND ";
        }
      }

    }

    $where = substr($where, 0, -5);
    $where_hris = substr($where_hris, 0, -5);
    if ($where != '') {
      $where = 'WHERE '.$where;
    }
    if ($where_hris != '') {
      $where_hris = 'WHERE '.$where_hris;
    }

    $sap ="
      WITH EMPLOYEE_INSTANCE AS (
        SELECT * FROM TM_EMPLOYEE_SAP
        WHERE $where_ins TRUNC(TO_DATE('".date('Y-m-d')."', 'YYYY-MM-DD'), 'MM') BETWEEN START_VALID AND
          CASE WHEN EXTRACT (YEAR FROM RES_DATE) != 9999
            THEN RES_DATE
            ELSE END_VALID
          END
        OFFSET $offset ROWS FETCH NEXT $limit ROWS ONLY
      )
      SELECT INS.WERKS, INS.PROF_NAME, INS.COMP_CODE, INS.EST_CODE, INS.AFD_CODE, INS.DEPARTEMEN, INS.NIK, INS.EMPLOYEE_NAME, INS.START_VALID, INS.END_VALID, INS.ENTRY_DATE, INS.RES_DATE, INS.JOB_CODE, INS.JOB_TYPE, INS.EMP_STAT, INS.EMP_STAT1, INS.ADDRESS, INS.POB, INS.DOB, INS.SEX, INS.RELIGION, INS.STATUS, INS.GANGCODE, INS.SPV_NIK, INS.NO_KTP, INS.NPWP, INS.FINGER_CODE, INS.GOLONGAN, INS.EMAIL, INS.PHONE, INS.TIME_ID, INS.PHASE, INS.CONDUCTOR_NAME, INS.CUSTOMER, INS.RESIDENT_FLAG, INS.IDENTIFICATION, INS.HOME_PHONE, INS.MOBILE_PHONE, INS.SALARY_TYPE, INS.POSITION, INS.LEVEL_EMPLOYEE, INS.CURRENCY, INS.SALARY, INS.INSENTIF, INS.RICE_PORTION_OPTIONS, INS.ORGANIZATION_CODE, INS.ASTEK_TYPE, INS.IC_NUMBER_OLD, INS.IC_NUMBER_NEW, INS.EPF_NUMBER, INS.SOCSO_NUMBER, INS.RACE, INS.PAYMENT_TYPE, INS.IC_SARAWAK_REGION, INS.PASSPORT_NUMBER, INS.PASSPORT_EXPIRE_DATE, INS.WORK_PERMIT_EXPIRE_DATE, INS.BOND_EXPIRE_DATE, INS.EPF_TYPE, INS.EPF_PERCENTAGE, INS.BANK_COUNTRY_KEY, INS.BANK_KEYS, INS.BANK_ACCOUNT_NUMBER, INS.DESCRIPTION, INS.EMPLOYEE_STATUS_INDICATOR, INS.NIK_OLD, INS.EXPIRE_DATE_CONTRACT, INS.INDCTR, INS.PERIOD_PROBATION, INS.CONTRACT_TO, INS.SPK_SK, INS.NO_KK, INS.BPJS_KETENAGAKERJAAN, INS.BPJS_KESEHATAN, INS.DOMISILI_ID, INS.LICENSE_ID, NULL AS NIK_NASIONAL, CC.COMP_CODE, CC.COMP_NAME, EE.EST_CODE, EE.EST_NAME, AA.AFD_CODE, AA.AFD_NAME, TJ.JOIN_DATE, 'SAP' AS DATA_SOURCE, DECODE(SUBSTR(EE.WERKS, 3, 1), '4', 'MILL', 'ESTATE') ESTATE_MILL
      FROM EMPLOYEE_INSTANCE INS JOIN TM_COMP CC ON CC.COMP_CODE = INS.COMP_CODE
      JOIN TM_EST EE ON EE.WERKS = INS.WERKS AND EE.COMP_CODE = CC.COMP_CODE
      LEFT JOIN (
        SELECT REGION_CODE, COMP_CODE, EST_CODE, AFD_CODE, AFD_NAME FROM TM_AFD
        WHERE TRUNC(TO_DATE('".date('Y-m-d')."', 'YYYY-MM-DD'), 'MM') BETWEEN START_VALID AND END_VALID
      ) AA ON AA.AFD_CODE = INS.AFD_CODE AND AA.EST_CODE = EE.EST_CODE AND AA.COMP_CODE = CC.COMP_CODE
      JOIN (
        SELECT J.NIK, J.START_VALID JOIN_DATE FROM TM_EMPLOYEE_SAP J WHERE J.START_VALID = (
          SELECT MIN(K.START_VALID) FROM TM_EMPLOYEE_SAP K WHERE K.NIK = J.NIK
        )
      ) TJ ON TJ.NIK = INS.NIK $where";
    $hris = "
      UNION
      SELECT NULL AS WERKS, NULL AS PROF_NAME, NULL AS COMP_CODE, NULL AS EST_CODE, NULL AS AFD_CODE, NULL AS DEPARTEMEN, EMPLOYEE_NIK AS NIK, EMPLOYEE_FULLNAME AS EMPLOYEE_NAME, EMPLOYEE_JOINDATE AS START_VALID, NULL AS END_VALID, NULL AS ENTRY_DATE, EMPLOYEE_RESIGNDATE AS RES_DATE, EMPLOYEE_DEPARTMENT AS JOB_CODE, EMPLOYEE_GRADE AS JOB_TYPE, NULL AS EMP_STAT, NULL AS EMP_STAT1, NULL AS ADDRESS, NULL AS POB, EMPLOYEE_BIRTHDAY AS DOB, DECODE(EMPLOYEE_GENDER, 'M', 'MALE', 'F', 'FEMALE', EMPLOYEE_GENDER) AS SEX, DECODE(EMPLOYEE_RELIGION, 'MOESLEM', 'ISLAM', 'CATHOLIC', 'KATOLIK', 'CHRISTIAN', 'PROTESTAN', NULL, 'N/A', '(NONE)', 'N/A', EMPLOYEE_RELIGION) AS RELIGION, NULL AS STATUS, NULL AS GANGCODE, EMPLOYEE_SPVNIK AS SPV_NIK, NULL AS NO_KTP, NULL AS NPWP, NULL AS FINGER_CODE, NULL AS GOLONGAN, EMPLOYEE_EMAIL AS EMAIL, NULL AS PHONE, NULL AS TIME_ID, NULL AS PHASE, NULL AS CONDUCTOR_NAME, NULL AS CUSTOMER, NULL AS RESIDENT_FLAG, NULL AS IDENTIFICATION, NULL AS HOME_PHONE, NULL AS MOBILE_PHONE, NULL AS SALARY_TYPE, NULL AS POSITION, NULL AS LEVEL_EMPLOYEE, NULL AS CURRENCY, NULL AS SALARY, NULL AS INSENTIF, NULL AS RICE_PORTION_OPTIONS, NULL AS ORGANIZATION_CODE, NULL AS ASTEK_TYPE, NULL AS IC_NUMBER_OLD, NULL AS IC_NUMBER_NEW, NULL AS EPF_NUMBER, NULL AS SOCSO_NUMBER, NULL AS RACE, NULL AS PAYMENT_TYPE, NULL AS IC_SARAWAK_REGION, NULL AS PASSPORT_NUMBER, NULL AS PASSPORT_EXPIRE_DATE, NULL AS WORK_PERMIT_EXPIRE_DATE, NULL AS BOND_EXPIRE_DATE, NULL AS EPF_TYPE, NULL AS EPF_PERCENTAGE, NULL AS BANK_COUNTRY_KEY, NULL AS BANK_KEYS, NULL AS BANK_ACCOUNT_NUMBER, NULL AS DESCRIPTION, NULL AS EMPLOYEE_STATUS_INDICATOR, NULL AS NIK_OLD, NULL AS EXPIRE_DATE_CONTRACT, NULL AS INDCTR, NULL AS PERIOD_PROBATION, NULL AS CONTRACT_TO, NULL AS SPK_SK, NULL AS NO_KK, NULL AS BPJS_KETENAGAKERJAAN, NULL AS BPJS_KESEHATAN, NULL AS DOMISILI_ID, NULL AS LICENSE_ID, NULL AS NIK_NASIONAL, EMPLOYEE_COMPANYCODE AS COMP_CODE, NULL AS COMP_NAME, NULL AS EST_CODE, NULL AS EST_NAME, NULL AS AFD_CODE, NULL AS AFD_NAME, EMPLOYEE_JOINDATE AS JOIN_DATE, 'HRIS' AS DATA_SOURCE, NULL AS ESTATE_MILL
      FROM TM_EMPLOYEE_HRIS $where_hris";

    if($union) {
      $sql = $sap.$hris;
    } else {
      $sql = $sap;
    }
    
    try {
      $ps = $this->tapdw->query($sql);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'][] = $this->query_helper->nullerRow($row, 'N/A');
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }
    $res->withHeader('Content-type', 'application/json');
    return $res->withJson($result);
  }
  */

  /**
   * Tampilkan informasi lengkap employee berdasarkan nik
   * @param  [String] NIK pegawai
   */
  /**
  public function getEmployee($req, $res, $arg)
  {
    if(!$arg['NIK']) {
      $res->withHeader('Content-type', 'application/json');
      return $res->withJson(['error' => true, 'status' => 500, 'message' => 'Parameter not satisfied']);
    }

    $sql ="
      WITH EMPLOYEE_INSTANCE AS (
        SELECT * FROM TM_EMPLOYEE_SAP
        WHERE TRUNC(TO_DATE('".date('Y-m-d')."', 'YYYY-MM-DD'), 'MM') BETWEEN START_VALID AND
          CASE WHEN EXTRACT (YEAR FROM RES_DATE) != 9999
            THEN RES_DATE
            ELSE END_VALID
          END
        AND NIK = '".$arg['NIK']."'
      )
      SELECT INS.WERKS, INS.PROF_NAME, INS.COMP_CODE, INS.EST_CODE, INS.AFD_CODE, INS.DEPARTEMEN, INS.NIK, INS.EMPLOYEE_NAME, INS.START_VALID, INS.END_VALID, INS.ENTRY_DATE, INS.RES_DATE, INS.JOB_CODE, INS.JOB_TYPE, INS.EMP_STAT, INS.EMP_STAT1, INS.ADDRESS, INS.POB, INS.DOB, INS.SEX, INS.RELIGION, INS.STATUS, INS.GANGCODE, INS.SPV_NIK, INS.NO_KTP, INS.NPWP, INS.FINGER_CODE, INS.GOLONGAN, INS.EMAIL, INS.PHONE, INS.TIME_ID, INS.PHASE, INS.CONDUCTOR_NAME, INS.CUSTOMER, INS.RESIDENT_FLAG, INS.IDENTIFICATION, INS.HOME_PHONE, INS.MOBILE_PHONE, INS.SALARY_TYPE, INS.POSITION, INS.LEVEL_EMPLOYEE, INS.CURRENCY, INS.SALARY, INS.INSENTIF, INS.RICE_PORTION_OPTIONS, INS.ORGANIZATION_CODE, INS.ASTEK_TYPE, INS.IC_NUMBER_OLD, INS.IC_NUMBER_NEW, INS.EPF_NUMBER, INS.SOCSO_NUMBER, INS.RACE, INS.PAYMENT_TYPE, INS.IC_SARAWAK_REGION, INS.PASSPORT_NUMBER, INS.PASSPORT_EXPIRE_DATE, INS.WORK_PERMIT_EXPIRE_DATE, INS.BOND_EXPIRE_DATE, INS.EPF_TYPE, INS.EPF_PERCENTAGE, INS.BANK_COUNTRY_KEY, INS.BANK_KEYS, INS.BANK_ACCOUNT_NUMBER, INS.DESCRIPTION, INS.EMPLOYEE_STATUS_INDICATOR, INS.NIK_OLD, INS.EXPIRE_DATE_CONTRACT, INS.INDCTR, INS.PERIOD_PROBATION, INS.CONTRACT_TO, INS.SPK_SK, INS.NO_KK, INS.BPJS_KETENAGAKERJAAN, INS.BPJS_KESEHATAN, INS.DOMISILI_ID, INS.LICENSE_ID, NULL AS NIK_NASIONAL, CC.COMP_CODE, CC.COMP_NAME, EE.EST_CODE, EE.EST_NAME, AA.AFD_CODE, AA.AFD_NAME, TJ.JOIN_DATE, 'SAP' AS DATA_SOURCE, DECODE(SUBSTR(EE.WERKS, 3, 1), '4', 'MILL', 'ESTATE') ESTATE_MILL
      FROM EMPLOYEE_INSTANCE INS JOIN TM_COMP CC ON CC.COMP_CODE = INS.COMP_CODE
      JOIN TM_EST EE ON EE.WERKS = INS.WERKS AND EE.COMP_CODE = CC.COMP_CODE
      LEFT JOIN (
        SELECT REGION_CODE, COMP_CODE, EST_CODE, AFD_CODE, AFD_NAME FROM TM_AFD
        WHERE TRUNC(TO_DATE('".date('Y-m-d')."', 'YYYY-MM-DD'), 'MM') BETWEEN START_VALID AND END_VALID
      ) AA ON AA.AFD_CODE = INS.AFD_CODE AND AA.EST_CODE = EE.EST_CODE AND AA.COMP_CODE = CC.COMP_CODE
      JOIN (
        SELECT J.NIK, J.START_VALID JOIN_DATE FROM TM_EMPLOYEE_SAP J WHERE J.START_VALID = (
          SELECT MIN(K.START_VALID) FROM TM_EMPLOYEE_SAP K WHERE K.NIK = J.NIK
        )
      ) TJ ON TJ.NIK = INS.NIK
      UNION
      SELECT NULL AS WERKS, NULL AS PROF_NAME, NULL AS COMP_CODE, NULL AS EST_CODE, NULL AS AFD_CODE, NULL AS DEPARTEMEN, EMPLOYEE_NIK AS NIK, EMPLOYEE_FULLNAME AS EMPLOYEE_NAME, EMPLOYEE_JOINDATE AS START_VALID, NULL AS END_VALID, NULL AS ENTRY_DATE, EMPLOYEE_RESIGNDATE AS RES_DATE, EMPLOYEE_DEPARTMENT AS JOB_CODE, EMPLOYEE_GRADE AS JOB_TYPE, NULL AS EMP_STAT, NULL AS EMP_STAT1, NULL AS ADDRESS, NULL AS POB, EMPLOYEE_BIRTHDAY AS DOB, DECODE(EMPLOYEE_GENDER, 'M', 'MALE', 'F', 'FEMALE', EMPLOYEE_GENDER) AS SEX, DECODE(EMPLOYEE_RELIGION, 'MOESLEM', 'ISLAM', 'CATHOLIC', 'KATOLIK', 'CHRISTIAN', 'PROTESTAN', NULL, 'N/A', '(NONE)', 'N/A', EMPLOYEE_RELIGION) AS RELIGION, NULL AS STATUS, NULL AS GANGCODE, EMPLOYEE_SPVNIK AS SPV_NIK, NULL AS NO_KTP, NULL AS NPWP, NULL AS FINGER_CODE, NULL AS GOLONGAN, EMPLOYEE_EMAIL AS EMAIL, NULL AS PHONE, NULL AS TIME_ID, NULL AS PHASE, NULL AS CONDUCTOR_NAME, NULL AS CUSTOMER, NULL AS RESIDENT_FLAG, NULL AS IDENTIFICATION, NULL AS HOME_PHONE, NULL AS MOBILE_PHONE, NULL AS SALARY_TYPE, NULL AS POSITION, NULL AS LEVEL_EMPLOYEE, NULL AS CURRENCY, NULL AS SALARY, NULL AS INSENTIF, NULL AS RICE_PORTION_OPTIONS, NULL AS ORGANIZATION_CODE, NULL AS ASTEK_TYPE, NULL AS IC_NUMBER_OLD, NULL AS IC_NUMBER_NEW, NULL AS EPF_NUMBER, NULL AS SOCSO_NUMBER, NULL AS RACE, NULL AS PAYMENT_TYPE, NULL AS IC_SARAWAK_REGION, NULL AS PASSPORT_NUMBER, NULL AS PASSPORT_EXPIRE_DATE, NULL AS WORK_PERMIT_EXPIRE_DATE, NULL AS BOND_EXPIRE_DATE, NULL AS EPF_TYPE, NULL AS EPF_PERCENTAGE, NULL AS BANK_COUNTRY_KEY, NULL AS BANK_KEYS, NULL AS BANK_ACCOUNT_NUMBER, NULL AS DESCRIPTION, NULL AS EMPLOYEE_STATUS_INDICATOR, NULL AS NIK_OLD, NULL AS EXPIRE_DATE_CONTRACT, NULL AS INDCTR, NULL AS PERIOD_PROBATION, NULL AS CONTRACT_TO, NULL AS SPK_SK, NULL AS NO_KK, NULL AS BPJS_KETENAGAKERJAAN, NULL AS BPJS_KESEHATAN, NULL AS DOMISILI_ID, NULL AS LICENSE_ID, NULL AS NIK_NASIONAL, EMPLOYEE_COMPANYCODE AS COMP_CODE, NULL AS COMP_NAME, NULL AS EST_CODE, NULL AS EST_NAME, NULL AS AFD_CODE, NULL AS AFD_NAME, EMPLOYEE_JOINDATE AS JOIN_DATE, 'HRIS' AS DATA_SOURCE, NULL AS ESTATE_MILL 
      FROM TM_EMPLOYEE_HRIS WHERE EMPLOYEE_NIK = '".$arg['NIK']."'";

    try {
      $ps = $this->tapdw->query($sql);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'] = $this->query_helper->nullerRow($row, 'N/A');
        $result['data']['CUTI'] = $this->sisaCuti($arg['NIK']);
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    $res->withHeader('Content-type', 'application/json');
    return $res->withJson($result);
  }
  */

  /**
   * Jumlah cuti dan mangkir karyawan sap
   */
  public function sisaCuti($nik)
  {
    $sql = "SELECT DECODE(ATTENDANCE_CODE, 'M', 'MANGKIR', 'C', 'CUTI') STATUS , COUNT(ATTENDANCE_CODE) JUMLAH
            FROM TM_EMPLOYEE_LAST_YEAR_ATTD 
            WHERE NIK = '$nik' AND ATTENDANCE_CODE IN ('C','M')
            AND EXTRACT(YEAR FROM ATTENDANCE_DATE) = '".date('Y')."'
            GROUP BY NIK, ATTENDANCE_CODE";

    try {
      $ps = $this->tapdw->query($sql);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result[$row['STATUS']] = (int)$row['JUMLAH'];
      }
      $result['CUTI'] = isset($result['CUTI']) ? $result['CUTI'] : 0;
      $result['MANGKIR'] = isset($result['MANGKIR']) ? $result['MANGKIR'] : 0;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }
    return $result;
  }

  public function getOvertime($nik)
  {

  }


  /**
   * Menampilkan data kehadiran dan produktifitas berdasarkan nik dan range bulan-tahun.
   * @param  [String]  nik          Employee harus dikirim dalam format urlencode
   * @param  [Date]    month_start  Tanggal terakhir kehadiran/productivity yang ingin ditampilkan
   * @param  [Integer] $arg         Jumlah bulan yang ingin ditampilkan datanya
   * @return [JSON]                 Data kehadiran dan produktifitas
   */
  public function getProductivity($req, $res, $arg)
  {
    if( !isset($arg['NIK']) || !isset($arg["date_start"]) || !isset($arg["date_end"]) ) {
      $res->withHeader('Content-type', 'application/json');
      return $res->withJson(['error' => true, 'status' => 500, 'message' => 'Parameter not satisfied']);
    }

    $month_start = date($arg["date_start"]);
    $month_end = $arg["date_end"];

    $q1 = "WITH MONTH_INTERVAL AS (SELECT TRUNC(TANGGAL, 'MM') - INTERVAL '$month_end' MONTH MONTH_START,
             LAST_DAY(TO_DATE(TRUNC(TANGGAL , 'MM') - INTERVAL '1' MONTH)) MONTH_END
             FROM (
               SELECT TO_DATE('".$month_start."', 'YYYY-MM-DD') AS TANGGAL FROM DUAL
             )
           )
           SELECT DISTINCT TO_CHAR(LAST_DAY(MONTH_START + DAYSTOADD), 'RRRRMM') MONTH_YEAR
           FROM MONTH_INTERVAL
           CROSS JOIN (
             SELECT LEVEL-1 DAYSTOADD FROM MONTH_INTERVAL CONNECT BY LEVEL <= (MONTH_END - MONTH_START)
           ) TGL ORDER BY 1 DESC";
    $spmon = [];
    try {
      $ps = $this->tapdw->query($q1);
      $ps->execute();
      $d = 1;
      while($row = $ps->fetchObject()) {
        $spmon['M'.$d++]    = floatval($row->MONTH_YEAR);
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    $pivot_header = '';
    foreach ($spmon as $k => $v) {
      $pivot_header .= "'$v' ". $k.', ';
    }

    $sql = "WITH 
            HV_ATTD AS (
              SELECT SUM(DAILY_PANEN) AS ATTD, YYYYMM YEARMONTH
              FROM (
                SELECT PLANT_CODE, POSTING_DATE, NIK, SUM(PANEN) H_PANEN, SUM(NON_PANEN) H_RAWAT, SUM(PANEN) + SUM(NON_PANEN) H_KERJA,
                CASE WHEN SUM(PANEN) != 0 THEN SUM(PANEN) ELSE SUM(NON_PANEN) END AS FACTOR, 
                ( SUM(PANEN) / (SUM(PANEN) + SUM(NON_PANEN)) ) AS DAILY_PANEN, YYYYMM
                FROM (
                  SELECT PLANT_CODE, POSTING_DATE, NIK, BLOCK_CODE, CASE WHEN ACTIVITY_NO = '5101030101' THEN COUNT(ACTIVITY_NO) ELSE 0 END AS PANEN,
                  CASE WHEN ACTIVITY_NO != '5101030101' THEN COUNT(ACTIVITY_NO) ELSE 0 END AS NON_PANEN, YYYYMM FROM TR_HARI_KERJA
                  WHERE POSTING_DATE BETWEEN TRUNC(TO_DATE('".$arg["date_start"]."', 'YYYY-MM-DD'), 'MM') - INTERVAL '".$arg["date_end"]."' MONTH 
                  AND LAST_DAY(TRUNC(TO_DATE('".$arg["date_start"]."', 'YYYY-MM-DD'), 'MM') - INTERVAL '1' MONTH)
                  AND NIK = '".$arg['NIK']."' AND BLOCK_CODE IS NOT NULL
                  GROUP BY PLANT_CODE, POSTING_DATE, NIK, ACTIVITY_NO, BLOCK_CODE, YYYYMM
                )
                GROUP BY PLANT_CODE, POSTING_DATE, NIK, YYYYMM
                ORDER BY POSTING_DATE ASC
              )
             GROUP BY YYYYMM
            ),
            PROD AS (
              SELECT YEARMONTH, SUM(PROD) / 1000 PRODUCTIVITY
              FROM (
                SELECT HVD.SUB_BLOCK_CODE, BJR.BJR_PREV, TO_CHAR(HVD.TANGGAL, 'YYYYMM') YEARMONTH, 
                SUM(HVD.QTY * BJR.BJR_PREV) PROD
                FROM (
                  SELECT SUB_BLOCK_CODE, AFD_CODE, NIK_PEMANEN, TANGGAL, WERKS, QTY
                  FROM TAP_DW.TR_HV_ESTATE_QUALITY_DAILY WHERE NIK_PEMANEN = '".$arg['NIK']."'
                  AND FLAG = 'HASIL PANEN' AND UOM = 'JJG'
                  AND TANGGAL BETWEEN TRUNC(TO_DATE('".$arg["date_start"]."', 'YYYY-MM-DD'), 'MM') - INTERVAL '".$arg["date_end"]."' MONTH 
                  AND LAST_DAY(TRUNC(TO_DATE('".$arg["date_start"]."', 'YYYY-MM-DD'), 'MM') - INTERVAL '1' MONTH)
                ) HVD
                JOIN (
                  SELECT WERKS, SUB_BLOCK_CODE, SPMON, BJR_PREV, AFD_CODE
                  FROM TAP_DW.TR_HV_BJR
                  WHERE SPMON BETWEEN TRUNC(TO_DATE('".$arg["date_start"]."', 'YYYY-MM-DD'), 'MM') - INTERVAL '".$arg["date_end"]."' MONTH 
                  AND LAST_DAY(TRUNC(TO_DATE('".$arg["date_start"]."', 'YYYY-MM-DD'), 'MM') - INTERVAL '1' MONTH)
                  AND WERKS = SUBSTR('".$arg['NIK']."', 4,4)
                ) BJR ON BJR.WERKS = HVD.WERKS AND BJR.SUB_BLOCK_CODE = HVD.SUB_BLOCK_CODE
                AND BJR.SPMON = LAST_DAY(TANGGAL) AND BJR.AFD_CODE = HVD.AFD_CODE
                GROUP BY HVD.SUB_BLOCK_CODE, BJR.BJR_PREV, TO_CHAR(HVD.TANGGAL, 'YYYYMM')
              )
              GROUP BY YEARMONTH
            ),
            ATTD AS (
              SELECT TO_CHAR(ATT.ATTENDANCE_DATE, 'YYYYMM') YEARMONTH, COUNT(ATT.ATTENDANCE_CODE) KEHADIRAN
              FROM TM_EMPLOYEE_LAST_YEAR_ATTD ATT
              JOIN ( SELECT * FROM TM_EMPLOYEE_SAP
                WHERE TRUNC(TO_DATE('".$arg["date_start"]."', 'YYYY-MM-DD'), 'MM') BETWEEN START_VALID AND
                CASE WHEN EXTRACT (YEAR FROM RES_DATE) != 9999 THEN RES_DATE ELSE END_VALID END
                AND NIK = '".$arg['NIK']."'
              ) EMP ON EMP.NIK = ATT.NIK WHERE ATT.ATTENDANCE_CODE LIKE 'K%'
              AND ATT.ATTENDANCE_DATE BETWEEN TRUNC(TO_DATE('".$arg["date_start"]."', 'YYYY-MM-DD'), 'MM') - INTERVAL '".$arg["date_end"]."' MONTH 
                AND LAST_DAY(TRUNC(TO_DATE('".$arg["date_start"]."', 'YYYY-MM-DD'), 'MM') - INTERVAL '1' MONTH)
              GROUP BY TO_CHAR(ATT.ATTENDANCE_DATE, 'YYYYMM')
            ),
            
            SPMON AS (
              SELECT *  FROM (
               ( SELECT ".substr($pivot_header, 0, -2)." FROM DUAL ) UNPIVOT ( BULAN FOR STRING IN (".implode(',', array_keys($spmon)).") ) 
              )
            )
            SELECT SPMON.BULAN, SPMON.STRING, NVL(ATTD.KEHADIRAN,0) KEHADIRAN, NVL(PROD.PRODUCTIVITY/HV_ATTD.ATTD,0) PRODUCTIVITY
            FROM SPMON
            LEFT JOIN ATTD ON ATTD.YEARMONTH = SPMON.BULAN
            LEFT JOIN PROD ON ATTD.YEARMONTH = PROD.YEARMONTH
            LEFT JOIN HV_ATTD ON HV_ATTD.YEARMONTH = PROD.YEARMONTH";

    $time_start = microtime(true);
    try {
      $ps = $this->tapdw->query($sql);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetchObject()) {
        $row->KEHADIRAN    = floatval($row->KEHADIRAN);
        $row->PRODUCTIVITY = floatval($row->PRODUCTIVITY);
        $result['data'][] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
      
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    $res->withHeader('Content-type', 'application/json');

    $time_end = microtime(true);
    $this->logger->debug('getProductivity :'. str_replace('  ', '', $sql), array('benchmark' => $time_end - $time_start) );

    return $res->withJson($result);
  }

  public function jabatan($req, $res, $arg)
  {
    $search_key = ["JOB_CODE" => "JOB_CODE","JOB_TYPE" => "JOB_TYPE","JOB_DESCRIPTION" => "JOB_DESCRIPTION","SOURCE" => "SOURCE"    ];

    $where = '';
    foreach ($arg as $key => $val) {
      if (array_key_exists($key, $search_key)) {
        $where .= $search_key[$key]." like '%".strtoupper($val)."%' AND ";
      }
    }

    if($where) {
      $where = 'WHERE '.substr($where, 0, -4);
    }

    $sql = "SELECT JOB_CODE, JOB_DESCRIPTION, JOB_TYPE, SOURCE FROM TM_JOB $where";

    try {
      $ps = $this->tapdw->query($sql);
      $ps->execute();

      $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'][] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }
    $res->withHeader('Content-type', 'application/json');

    return $res->withJson($result);
  }

  public function getColumnName($table_name, $connection = null)
  {
    if($connection) {
      $this->db = $connection;
    } else {
      $this->db = $this->tapdw;
    }
    $sql = "SELECT column_name as COLUMN_NAME FROM all_tab_columns WHERE table_name = '$table_name' ORDER BY 1";
    try {
      $ps = $this->db->query($sql);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result[] = $row['COLUMN_NAME'];
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }
    return $result;
  }
  
  /*aries 
	function untuk melakukan auto-complete filed nama karyawan dari SAP (MANDOR) dan HRIS*/
  public function autoEmployee($req, $res, $arg)
  {
    if(!$arg['EMPLOYEE_NAME'] || !$arg['WERKS']) {
      $res->withHeader('Content-type', 'application/json');
      return $res->withJson(['error' => true, 'status' => 500, 'message' => 'Parameter not satisfied']);
    }
    $NAME = strtoupper($arg['EMPLOYEE_NAME']);
    $werks = $arg['WERKS'];

    $sql = "WITH EMPLOYEE_AUTO_INSTANCE AS (
			 SELECT NIK, EMPLOYEE_NAME, JOB_CODE, 'SAP' AS TBL FROM TM_EMPLOYEE_SAP
					WHERE JOB_CODE LIKE '%MANDOR%' AND WERKS = '".$werks."'
					AND TRUNC(TO_DATE('".date('Y-m-d')."', 'YYYY-MM-DD'), 'MM') BETWEEN START_VALID AND
					  CASE WHEN EXTRACT (YEAR FROM RES_DATE) != 9999
						THEN RES_DATE
						ELSE END_VALID
					  END
			 UNION
				  SELECT EMPLOYEE_NIK AS NIK, EMPLOYEE_FULLNAME AS EMPLOYEE_NAME, EMPLOYEE_POSITION AS JOB_CODE, 'HRIS' AS TBL
				  FROM TM_EMPLOYEE_HRIS WHERE EMPLOYEE_RESIGNDATE IS NULL
				  )
				  SELECT * FROM EMPLOYEE_AUTO_INSTANCE WHERE EMPLOYEE_NAME LIKE '%".$NAME."%'";
	
	try {
    $ps = $this->tapdw->query($sql);
    $ps->execute();

    $data_displayed = 0;
      while($row = $ps->fetch()) {
        $result['data'][] = $row;
        $data_displayed ++;
      }
      $result['count'] = $data_displayed;
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    $res->withHeader('Content-type', 'application/json');
    return $res->withJson($result);   
  }
}
