<?php

namespace Modules\Helpers;

class QueryHelper {

  /**
   * Generate where filters
   */
  public function generateFilters($key, $val, $column_name)
  {
    if(strpos($val, '|')) {
      $val = explode('|', $val);
    }
      if (is_array($val)) {
        $filter = strtoupper($column_name)." IN ('".strtoupper(implode("','", $val))."') AND ";
      } else {
        $filter = strtoupper($column_name)." LIKE '%".strtoupper($val)."%' AND ";
      }
    return $filter;
  }

  public function getColumnName($table_name, $connection)
  {
    $sql = "SELECT column_name as COLUMN_NAME FROM all_tab_columns WHERE table_name = '$table_name' ORDER BY 1";
    try {
      $ps = $connection->query($sql);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result[] = $row['COLUMN_NAME'];
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }
    return $result;
  }

  public function nullerRow($data, $null_value_tobe_nulled)
  {
    foreach ($data as $key => $val) {
      $data[$key] = $this->nuller($val, $null_value_tobe_nulled);
    }

    return $data;
  }

  public function nuller($val, $null_value_tobe_nulled)
  {
      if($val == $null_value_tobe_nulled) {
        $result = null;
      } else {
        $result = $val;
      }
      return $result;
  }

  public function fieldAlias($array_field)
  {
    $selected_fields = '';
    foreach ($array_field as $key => $value) {
      $selected_fields .= $value .' AS '. $key.',';
    }
    return substr($selected_fields, 0, -1);
  }

}
