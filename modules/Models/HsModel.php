<?php

namespace Modules\Models;

/**
 * Hectare State Model
 */
class HsModel
{

  protected $tapdw;
  protected $logger;

  function __construct($c)
  {
    $this->logger = $c['logger'];
    $this->tapdw = $c['tapdw'];
  }

  /**
   * Nilai BBC dalam rentang x bulan terakhir mulai dari current month - 1
   */
  public function getBBC($ba_code, $date) {
    $q = "WITH MONTH_INTERVAL AS (
          SELECT TRUNC(TO_DATE(TANGGAL - NUMTOYMINTERVAL( F_SPMON_INTERVAL(TANGGAL ,'TOP'), 'MONTH' )), 'MM') MONTH_START,
          LAST_DAY(TO_DATE(TANGGAL + NUMTOYMINTERVAL( F_SPMON_INTERVAL(TANGGAL ,'BOTTOM'), 'MONTH' ))) MONTH_END
          FROM (
            SELECT TO_DATE('".$date."', 'YYYY-MM-DD') AS TANGGAL FROM DUAL
          )
        )
        SELECT CAL.*, NVL(SUM(KG_JJG)/1000, 0) BBC FROM (
          SELECT DISTINCT LAST_DAY(MONTH_START + DAYSTOADD) TANGGAL, 
          TO_CHAR(LAST_DAY(MONTH_START + DAYSTOADD), 'RRRRMM') MONTH_YEAR
          FROM MONTH_INTERVAL
          CROSS JOIN (
            SELECT LEVEL-1 DAYSTOADD FROM MONTH_INTERVAL CONNECT BY LEVEL <= (MONTH_END - MONTH_START)
          ) TGL
        ) CAL
        LEFT JOIN TR_HV_BBC ON TR_HV_BBC.SPMON = CAL.TANGGAL AND TR_HV_BBC.WERKS = '$ba_code'
        GROUP BY CAL.TANGGAL, CAL.MONTH_YEAR
        ORDER BY 2";

    // $q = "WITH MONTH_INTERVAL AS (
    //         SELECT TRUNC(TO_DATE('".$date."', 'YYYY-MM-DD'), 'MM') - INTERVAL '4' MONTH MONTH_START,
    //         LAST_DAY(TRUNC(TO_DATE('".$date."', 'YYYY-MM-DD'),'MM') - INTERVAL '1' MONTH) MONTH_END FROM DUAL
    //       )
    //       SELECT NVL(SUM(KG_JJG)/1000, 0) BBC, TO_CHAR(TANGGAL, 'RRRRMM') MONTH_YEAR
    //       FROM (
    //         SELECT DISTINCT LAST_DAY(MONTH_START + DaysToAdd) TANGGAL FROM MONTH_INTERVAL
    //         CROSS JOIN (
    //           SELECT LEVEL-1 DaysToAdd FROM MONTH_INTERVAL
    //           CONNECT BY LEVEL <= (MONTH_END - MONTH_START)
    //         ) TGL
    //         WHERE MONTH_INTERVAL.MONTH_END - MONTH_INTERVAL.MONTH_START + 1 > DaysToAdd
    //       ) EOM
    //       LEFT JOIN TR_HV_BBC ON TR_HV_BBC.SPMON = EOM.TANGGAL AND TR_HV_BBC.WERKS = '".$ba_code."'
    //       GROUP BY TO_CHAR(TANGGAL, 'RRRRMM') ORDER BY 2";

    $time_start = microtime(true);
    $result = [];
    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result[$row['MONTH_YEAR']] = floatval($row["BBC"]);
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
    }

    $time_end = microtime(true);
    $this->logger->debug('getBBC :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );
    return $result;
  }

  public function getProduksi($ba_code, $year_month) {
    $q = "SELECT SUM (KG_PRODUKSI) / 1000 PRODUKSI
          FROM TR_HV_PRODUCTION_MONTHLY WHERE WERKS = '$ba_code'
          AND TO_CHAR(SPMON, 'MON-YY') = TO_CHAR(TRUNC(TO_DATE('".$year_month."', 'YYYY-MM-DD'),'MM') - INTERVAL '1' MONTH, 'MON-YY')";

    $time_start = microtime(true);
    $result = [];
    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result = floatval($row['PRODUKSI']);
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    $time_end = microtime(true);
    $this->logger->debug('getProduksi :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );
    return $result;
  }

  public function getTmTbm($ba_code, $year_month, $status) {
    ($status == 'TM') ? $status = "= 'TM'" : $status = "!= 'TM'";
    // $q = "SELECT SUM(HA_SAP) AMOUNT FROM TR_HS_LAND_USE WHERE WERKS = '$ba_code' 
    //       AND MATURITY_STATUS $status
    //       AND TRUNC(SPMON, 'MM') = TRUNC(TO_DATE('".$year_month."', 'YYYY-MM-DD'), 'MM') - INTERVAL '1' MONTH";

    $q = "WITH LAND_USAGE AS (
            SELECT MON.SPMON, SUM(HA_SAP) LASTMONTH, LAG(SUM(HA_SAP)) OVER (ORDER BY MON.SPMON) LAST2MONTH
            FROM TAP_DW.TM_TIME_MONTHLY MON
            JOIN TAP_DW.TR_HS_LAND_USE LU ON LU.WERKS = MON.WERKS AND LU.SPMON = MON.SPMON AND LU.MATURITY_STATUS ".$status."
            WHERE MON.WERKS = '".$ba_code."' AND
            MON.SPMON BETWEEN TRUNC(TO_DATE('".$year_month."', 'YYYY-MM-DD'), 'MM') - INTERVAL '2' MONTH
            AND LAST_DAY(TRUNC(TO_DATE('".$year_month."', 'YYYY-MM-DD'), 'MM') - INTERVAL '1' MONTH )
            GROUP BY MON.SPMON
          ) SELECT DECODE(LASTMONTH, NULL, LAST2MONTH, LASTMONTH) AMOUNT FROM 
          LAND_USAGE AA WHERE SPMON = (SELECT MAX(BB.SPMON) FROM LAND_USAGE BB)";

    $time_start = microtime(true);
    $result = [];
    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result = floatval($row["AMOUNT"]);
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    $time_end = microtime(true);
    $this->logger->debug('getTmTbm :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );
    return $result;
  }


  public function getHaTanam($ba_code, $year_month) {
    // $q = "SELECT SUM(HA_SAP) HA_TANAM FROM TR_HS_LAND_USE WHERE WERKS = '$ba_code'
    //       AND SPMON = LAST_DAY(TRUNC(TO_DATE('$year_month', 'YYYY-MM-DD'),'MM') - INTERVAL '1' MONTH) AND LAND_CAT = 'PLANTED'";
    $q = "WITH LAND_USAGE AS (
            SELECT MON.SPMON, SUM(HA_SAP) LASTMONTH, LAG(SUM(HA_SAP)) OVER (ORDER BY MON.SPMON) LAST2MONTH
            FROM TAP_DW.TM_TIME_MONTHLY MON
            JOIN TAP_DW.TR_HS_LAND_USE LU ON LU.WERKS = MON.WERKS AND LU.SPMON = MON.SPMON AND LU.LAND_CAT = 'PLANTED'
            WHERE MON.WERKS = '".$ba_code."' AND
            MON.SPMON BETWEEN TRUNC(TO_DATE('".$year_month."', 'YYYY-MM-DD'), 'MM') - INTERVAL '2' MONTH
            AND LAST_DAY(TRUNC(TO_DATE('".$year_month."', 'YYYY-MM-DD'), 'MM') - INTERVAL '1' MONTH )
            GROUP BY MON.SPMON
          ) SELECT DECODE(LASTMONTH, NULL, LAST2MONTH, LASTMONTH) HA_TANAM FROM 
          LAND_USAGE AA WHERE SPMON = (SELECT MAX(BB.SPMON) FROM LAND_USAGE BB)";

    $time_start = microtime(true);
    $result = [];
    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result = floatval($row['HA_TANAM']);
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
    }

    $time_end = microtime(true);
    $this->logger->debug('getHaTanam :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );
    return $result;
  }

  public function getHaPanen($ba_code, $date) {
    // $q = "SELECT SUM(HA_SAP) HA_PANEN FROM TR_HS_LAND_USE WHERE WERKS = '$ba_code'
    //       AND SPMON = LAST_DAY(TRUNC(TO_DATE('".$date."', 'YYYY-MM-DD'),'MM') - INTERVAL '1' MONTH)
    //       AND (MATURITY_STATUS = 'TM' OR SCOUT_STATUS = 'SCOUT')";

    $q = "WITH LAND_USAGE AS (
            SELECT MON.SPMON, SUM(HA_SAP) LASTMONTH, LAG(SUM(HA_SAP)) OVER (ORDER BY MON.SPMON) LAST2MONTH
            FROM TAP_DW.TM_TIME_MONTHLY MON
            JOIN TAP_DW.TR_HS_LAND_USE LU ON LU.WERKS = MON.WERKS
            AND LU.SPMON = MON.SPMON AND (LU.MATURITY_STATUS = 'TM' OR LU.SCOUT_STATUS = 'SCOUT')
            WHERE MON.WERKS = '".$ba_code."' AND
            MON.SPMON BETWEEN TRUNC(TO_DATE('".$date."', 'YYYY-MM-DD'), 'MM') - INTERVAL '2' MONTH
            AND LAST_DAY(TRUNC(TO_DATE('".$date."', 'YYYY-MM-DD'), 'MM') - INTERVAL '1' MONTH )
            GROUP BY MON.SPMON
          ) SELECT DECODE(LASTMONTH, NULL, LAST2MONTH, LASTMONTH) HA_PANEN FROM 
          LAND_USAGE AA WHERE SPMON = (SELECT MAX(BB.SPMON) FROM LAND_USAGE BB)";

    $time_start = microtime(true);
    $result = [];
    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result = floatval($row["HA_PANEN"]);
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
    }

    $time_end = microtime(true);
    $this->logger->debug('getHaPanen :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );
    return $result;
  }

  // Man Power
  public function getMPE($ba_code, $date)
  {
    $yyyymm = date('Ym', strtotime("-1 month", strtotime(str_replace('-','/', $date))));

    $q = "
      SELECT COUNT(EMP.NIK) JUMLAH_MPE FROM TM_EMPLOYEE_SAP EMP
      JOIN (
        SELECT NIK
        FROM TM_EMPLOYEE_LAST_YEAR_ATTD WHERE WERKS = '$ba_code'
        AND ATTENDANCE_CODE IN ('K','KT','KL')
        AND TO_CHAR(ATTENDANCE_DATE, 'RRRRMM') = TO_CHAR(TRUNC((TO_DATE('".$date."', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH,'RRRRMM')
        GROUP BY NIK
      ) ATT ON ATT.NIK = EMP.NIK
      WHERE JOB_CODE = 'PEMANEN'
      AND TRUNC((TO_DATE('$date', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH BETWEEN START_VALID AND
        CASE WHEN EXTRACT (YEAR FROM RES_DATE) != 9999
          THEN RES_DATE
          ELSE END_VALID
        END
      AND WERKS = '$ba_code'
    ";

    $time_start = microtime(true);
    $result = [];
    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result = (int) $row["JUMLAH_MPE"];
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
    }

    $time_end = microtime(true);
    $this->logger->debug('getMPE :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );
    return $result;
  }

  // Pemanen based on status
  public function getPemanenBasedOnStatus($ba_code, $date, $status)
  {
    $status = "('".implode("','", $status)."')";
    $yyyymm = date('Ym', strtotime("-1 month", strtotime(str_replace('-','/', $date))));

    $q = "
      SELECT COUNT(EMP.NIK) JUMLAH_MPE FROM TM_EMPLOYEE_SAP EMP
      JOIN (
        SELECT DISTINCT NIK
        FROM TM_EMPLOYEE_LAST_YEAR_ATTD WHERE WERKS = '$ba_code'
        AND ATTENDANCE_CODE IN ('K','KT','KL')
        AND TO_CHAR(ATTENDANCE_DATE, 'RRRRMM') = TO_CHAR(TRUNC((TO_DATE('".$date."', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH,'RRRRMM')
      ) ATT ON ATT.NIK = EMP.NIK
      WHERE JOB_CODE = 'PEMANEN' AND STATUS IN $status
      AND TRUNC((TO_DATE('$date', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH BETWEEN START_VALID AND
        CASE WHEN EXTRACT (YEAR FROM RES_DATE) != 9999
          THEN RES_DATE
          ELSE END_VALID
        END
      AND WERKS = '$ba_code'
    ";

    $time_start = microtime(true);
    $result = [];
    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result = (int) $row["JUMLAH_MPE"];
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
    }

    $time_end = microtime(true);
    $this->logger->debug('getPemanenBasedOnStatus :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );
    return $result;
  }

  public function getMpeBasedOnStatus($ba_code, $date, $job, $status)
  {
    $status = "('".implode("','", $status)."')";
    $where  = $job['operator'];
    $where .= $job['prefix'];
    $where .= $job['job_code'];
    $where .= $job['suffix'];

    $q = "SELECT COUNT(EMP.NIK) JUMLAH_MPE FROM TM_EMPLOYEE_SAP EMP
          JOIN (
            SELECT DISTINCT NIK
            FROM TM_EMPLOYEE_LAST_YEAR_ATTD WHERE WERKS = '$ba_code'
            AND ATTENDANCE_CODE IN ('K','KT','KL')
            AND TO_CHAR(ATTENDANCE_DATE, 'RRRRMM') = TO_CHAR(TRUNC((TO_DATE('".$date."', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH,'RRRRMM')
          ) ATT ON ATT.NIK = EMP.NIK

          WHERE JOB_CODE $where
            AND STATUS IN $status
            AND TRUNC((TO_DATE('".$date."', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH BETWEEN START_VALID AND
              CASE WHEN EXTRACT (YEAR FROM RES_DATE) != 9999
                THEN RES_DATE
                ELSE END_VALID
              END
            AND WERKS = '$ba_code'";

    $time_start = microtime(true);
    $result = [];
    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result = (int) $row["JUMLAH_MPE"];
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
    }

    $time_end = microtime(true);
    $this->logger->debug('getMpeBasedOnStatus :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );
    return $result;
  }

  public function getMpeBasedOnJob($ba_code, $date, $job)
  {
    $where  = $job['operator'];
    $where .= $job['prefix'];
    $where .= $job['job_code'];
    $where .= $job['suffix'];

    $q = "WITH MPE_SAP AS (
            SELECT COUNT(EMP.NIK) JUMLAH_MPE, EMP.STATUS FROM TM_EMPLOYEE_SAP EMP
            JOIN (
              SELECT DISTINCT NIK
              FROM TM_EMPLOYEE_LAST_YEAR_ATTD WHERE WERKS = '$ba_code'
              AND ATTENDANCE_CODE IN ('K','KT','KL')
              AND TO_CHAR(ATTENDANCE_DATE, 'RRRRMM') = TO_CHAR(TRUNC((TO_DATE('".$date."', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH,'RRRRMM')
            ) ATT ON ATT.NIK = EMP.NIK
            WHERE JOB_CODE $where
            AND TRUNC((TO_DATE('".$date."', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH BETWEEN START_VALID AND
              CASE WHEN EXTRACT (YEAR FROM RES_DATE) != 9999
                THEN RES_DATE
                ELSE END_VALID
              END
            AND WERKS = '$ba_code' GROUP BY EMP.STATUS),
          MPE_HRIS AS (
            SELECT COUNT(EMPLOYEE_NIK) JUMLAH_MPE, ".$job['job_code']." STATUS FROM TM_EMPLOYEE_HRIS
            WHERE EMPLOYEE_POSITION $where
          )
          SELECT * FROM MPE_SAP UNION SELECT * FROM MPE_HRIS";

    $time_start = microtime(true);
    $result = [];
    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result[$row["STATUS"]] = (int) $row["JUMLAH_MPE"];
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
    }

    $time_end = microtime(true);
    $this->logger->debug('getMpeBasedOnJob :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );
    return $result;
  }

  public function getMandor($ba_code, $date)
  {
    $q = "WITH MANDOR AS (
            SELECT COUNT(EMP.NIK) JUMLAH_MPE, JOB_CODE FROM TM_EMPLOYEE_SAP EMP
            JOIN ( SELECT DISTINCT NIK FROM TM_EMPLOYEE_LAST_YEAR_ATTD WHERE WERKS = '$ba_code' AND ATTENDANCE_CODE IN ('K','KT','KL')
              AND TO_CHAR(ATTENDANCE_DATE, 'RRRRMM') = TO_CHAR(TRUNC((TO_DATE('$date', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH,'RRRRMM')
              ) ATT ON ATT.NIK = EMP.NIK
            WHERE JOB_CODE IN ('MANDOR RAWAT', 'MANDOR PANEN','MANDOR 1')
            AND TRUNC((TO_DATE('$date', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH BETWEEN START_VALID AND
              CASE
                WHEN EXTRACT (YEAR FROM RES_DATE) != 9999
                THEN RES_DATE
                ELSE END_VALID
              END
            AND WERKS = '$ba_code' GROUP BY JOB_CODE
          ),
          MANDOR_UMUM AS
          (SELECT COUNT(EMP.NIK) JUMLAH_MPE,'MANDOR UMUM' JOB_CODE FROM TM_EMPLOYEE_SAP EMP
          JOIN ( SELECT DISTINCT NIK FROM TM_EMPLOYEE_LAST_YEAR_ATTD WHERE WERKS = '$ba_code'
            AND ATTENDANCE_CODE IN ('K','KT','KL')
            AND TO_CHAR(ATTENDANCE_DATE, 'RRRRMM') = TO_CHAR(TRUNC((TO_DATE('$date', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH,'RRRRMM')
            ) ATT ON ATT.NIK = EMP.NIK
          WHERE JOB_CODE NOT IN('MANDOR PANEN', 'MANDOR RAWAT', 'MANDOR 1')
          AND JOB_CODE LIKE 'MANDOR%'
          AND TRUNC((TO_DATE('$date', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH BETWEEN START_VALID AND
            CASE
              WHEN EXTRACT (YEAR FROM RES_DATE) != 9999
              THEN RES_DATE
              ELSE END_VALID
            END
          AND WERKS = '$ba_code'
          )
        SELECT JUMLAH_MPE, JOB_CODE FROM MANDOR UNION ALL SELECT JUMLAH_MPE, JOB_CODE FROM MANDOR_UMUM";

    $time_start = microtime(true);
    $result = [];
    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result[$row['JOB_CODE']] = (int) $row['JUMLAH_MPE'];
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
    }

    $time_end = microtime(true);
    $this->logger->debug('getMandor :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );
    return $result;
  }

  public function getMpePekerja($ba_code, $date)
  {
    $q = "WITH MPE_SAP AS (
            SELECT COUNT(EMP.NIK) JUMLAH_MPE, JOB_CODE FROM TM_EMPLOYEE_SAP EMP
            JOIN (
              SELECT DISTINCT NIK
              FROM TM_EMPLOYEE_LAST_YEAR_ATTD WHERE WERKS = '$ba_code'
              AND ATTENDANCE_CODE IN ('K','KT','KL')
              AND TO_CHAR(ATTENDANCE_DATE, 'RRRRMM') = TO_CHAR(TRUNC((TO_DATE('".$date."', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH,'RRRRMM')
            ) ATT ON ATT.NIK = EMP.NIK
            WHERE JOB_CODE IN ('PEKERJA RAWAT', 'PEMANEN')
            AND TRUNC((TO_DATE('".$date."', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH BETWEEN START_VALID AND
              CASE WHEN EXTRACT (YEAR FROM RES_DATE) != 9999
                THEN RES_DATE
                ELSE END_VALID
              END
            AND WERKS = '$ba_code' GROUP BY JOB_CODE
          ), 
          MPE_UMUM AS (
            SELECT COUNT(EMP.NIK) JUMLAH_MPE, 'PEKERJA UMUM' AS JOB_CODE FROM TM_EMPLOYEE_SAP EMP
            JOIN (
              SELECT DISTINCT NIK
              FROM TM_EMPLOYEE_LAST_YEAR_ATTD WHERE WERKS = '$ba_code'
              AND ATTENDANCE_CODE IN ('K','KT','KL')
              AND TO_CHAR(ATTENDANCE_DATE, 'RRRRMM') = TO_CHAR(TRUNC((TO_DATE('".$date."', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH,'RRRRMM')
            ) ATT ON ATT.NIK = EMP.NIK
            WHERE 
            JOB_CODE NOT IN('PEMANEN', 'PEKERJA RAWAT') AND JOB_CODE NOT LIKE 'MANDOR%'
            AND TRUNC(TO_DATE('".$date."', 'YYYY-MM-DD'), 'MM') - INTERVAL '1' MONTH BETWEEN START_VALID AND
              CASE WHEN EXTRACT (YEAR FROM RES_DATE) != 9999
                THEN RES_DATE
                ELSE END_VALID
              END
            AND WERKS = '$ba_code'
          )
          SELECT * FROM MPE_SAP UNION ALL SELECT * FROM MPE_UMUM";

    $time_start = microtime(true);
    $result = [];
    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result[$row["JOB_CODE"]] = (int) $row["JUMLAH_MPE"];
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
    }

    $time_end = microtime(true);
    $this->logger->debug('getMpePekerja :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );
    return $result;
  }

  public function getKehadiran($ba_code, $date, $job)
  {
    $bulan = date('m', strtotime("last month",strtotime($date)));
    $tahun = date('Y', strtotime("last month",strtotime($date)));
    $spmon = date('M-y', strtotime("last month",strtotime($date)));
    $q = "SELECT NVL(ROUND((SUM(HKB)/SUM(HKE))* 100,2), 0) KEHADIRAN FROM (
            SELECT ATT.NIK, COUNT(ATT.NIK) HKB
            FROM (
              SELECT NIK FROM TM_EMPLOYEE_SAP WHERE JOB_CODE = '$job'
              AND TRUNC(TO_DATE('$date', 'YYYY-MM-DD'), 'MM') - INTERVAL '1' MONTH BETWEEN START_VALID AND
                CASE WHEN EXTRACT (YEAR FROM RES_DATE) != 9999
                  THEN RES_DATE
                  ELSE END_VALID
                END
              AND WERKS = '$ba_code'
            ) EMP
            JOIN TM_EMPLOYEE_LAST_YEAR_ATTD ATT ON EMP.NIK = ATT.NIK
            JOIN TM_TIME_DAILY CAL ON CAL.TANGGAL = ATT.ATTENDANCE_DATE AND CAL.WERKS = ATT.WERKS AND CAL.FLAG_HK = 'Y'
            WHERE ATT.WERKS = '$ba_code'
            AND TRUNC(ATTENDANCE_DATE, 'MM') = TRUNC(TO_DATE('$date', 'YYYY-MM-DD'),'MM') - INTERVAL '1' MONTH
            AND ATTENDANCE_CODE IN ('K','KT','KL')
            GROUP BY ATT.NIK
          ), (
            SELECT SPMON, NVL(HKE,0) HKE FROM TM_TIME_MONTHLY
            WHERE NO_BULAN = EXTRACT(MONTH FROM (TRUNC(TO_DATE('$date', 'YYYY-MM-DD'),'MM') - INTERVAL '1' MONTH))
            AND TAHUN = EXTRACT(YEAR FROM (TRUNC(TO_DATE('$date', 'YYYY-MM-DD'),'MM') - INTERVAL '1' MONTH))
            AND WERKS = '$ba_code'
          )";

    $time_start = microtime(true);
    $result = [];
    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result = floatval($row["KEHADIRAN"]);
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
    }

    $time_end = microtime(true);
    $this->logger->debug('getKehadiran :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );
    return $result;
  }

  public function getKehadiranBasedOnStatus($ba_code, $date, $job, $status)
  {
    $status = "('".implode("','", $status)."')";
    $q = "SELECT NVL(ROUND((SUM(HKB)/SUM(HKE))* 100,2), 0) KEHADIRAN FROM (
        SELECT ATT.NIK, COUNT(ATT.NIK) HKB
        FROM (
          SELECT NIK FROM TM_EMPLOYEE_SAP WHERE JOB_CODE = '$job' AND STATUS IN $status
          AND TRUNC(TO_DATE('$date', 'YYYY-MM-DD'), 'MM') - INTERVAL '1' MONTH BETWEEN START_VALID AND
            CASE WHEN EXTRACT (YEAR FROM RES_DATE) != 9999
              THEN RES_DATE
              ELSE END_VALID
            END
          AND WERKS = '$ba_code'
        ) EMP
        JOIN TM_EMPLOYEE_LAST_YEAR_ATTD ATT ON EMP.NIK = ATT.NIK
        JOIN TM_TIME_DAILY CAL ON CAL.TANGGAL = ATT.ATTENDANCE_DATE AND CAL.WERKS = ATT.WERKS AND CAL.FLAG_HK = 'Y'
        WHERE ATT.WERKS = '$ba_code'
        AND TRUNC(ATTENDANCE_DATE, 'MM') = TRUNC(TO_DATE('$date', 'YYYY-MM-DD'),'MM') - INTERVAL '1' MONTH
        AND ATTENDANCE_CODE IN ('K','KT','KL')
        GROUP BY ATT.NIK
      ), (
        SELECT SPMON, NVL(HKE,0) HKE FROM TM_TIME_MONTHLY
        WHERE NO_BULAN = EXTRACT(MONTH FROM (TRUNC(TO_DATE('$date', 'YYYY-MM-DD'),'MM') - INTERVAL '1' MONTH))
        AND TAHUN = EXTRACT(YEAR FROM (TRUNC(TO_DATE('$date', 'YYYY-MM-DD'),'MM') - INTERVAL '1' MONTH))
        AND WERKS = '$ba_code'
      )";

    $time_start = microtime(true);
    $result = [];
    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result = floatval($row["KEHADIRAN"]);
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
    }

    $time_end = microtime(true);
    $this->logger->debug('getKehadiranBasedOnStatus :'. str_replace('  ', '', $q), array('benchmark' => $time_end - $time_start) );
    return $result;
  }

  public function getProduktifitas($ba_code, $date)
  {
    $sql = "WITH HKE AS (
              SELECT SUM(HK) PANEN FROM TR_HV_PRODUCTIVITY_DAILY WHERE
              TGL_PANEN BETWEEN TRUNC((TO_DATE('".$date."', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH
              AND LAST_DAY(TRUNC((TO_DATE('".$date."', 'YYYY-MM-DD')), 'MM')- INTERVAL '1' MONTH) 
              AND WERKS = '$ba_code'
            ), 
            TONASE AS (
              SELECT SUM(KG_PRODUKSI)/1000 TON FROM TR_HV_PRODUCTION_MONTHLY WHERE WERKS = '$ba_code'
              AND TO_CHAR(SPMON, 'RRRRMM') = TO_CHAR(TRUNC((TO_DATE('".$date."', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH,'RRRRMM')
            )
            SELECT TON/PANEN AS PRODUKTIFITAS
            FROM TONASE, HKE";
    // $sql = "WITH HITUNG AS (
    //           SELECT PLANT_CODE, POSTING_DATE, NIK, BLOCK_CODE,
    //           CASE WHEN ACTIVITY_NO = '5101030101' THEN COUNT(ACTIVITY_NO) ELSE 0 END AS PANEN,
    //           CASE WHEN ACTIVITY_NO != '5101030101' THEN COUNT(ACTIVITY_NO) ELSE 0 END AS NON_PANEN
    //           FROM TR_HARI_KERJA
    //           WHERE YYYYMM = CAST(TO_CHAR(TRUNC((TO_DATE('".$date."', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH,'RRRRMM') AS INT)
    //           AND PLANT_CODE = '$ba_code'
    //           AND NIK IS NOT NULL
    //           AND BLOCK_CODE IS NOT NULL
    //           GROUP BY PLANT_CODE, POSTING_DATE, NIK, ACTIVITY_NO, BLOCK_CODE
    //         ), 
    //         TONASE AS (
    //           SELECT SUM(KG_PRODUKSI)/1000 TON FROM TR_HV_PRODUCTION_MONTHLY WHERE WERKS = '$ba_code'
    //           AND TO_CHAR(SPMON, 'RRRRMM') = TO_CHAR(TRUNC((TO_DATE('".$date."', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH,'RRRRMM')
    //         )
    //         SELECT TON/PANEN AS PRODUKTIFITAS
    //         FROM TONASE, 
    //         (
    //           SELECT SUM(DAILY_PANEN) AS PANEN FROM ( 
    //             SELECT PLANT_CODE, POSTING_DATE, NIK, SUM(PANEN) H_PANEN, SUM(NON_PANEN) H_RAWAT,
    //             SUM(PANEN) + SUM(NON_PANEN) H_KERJA,
    //             CASE WHEN SUM(PANEN) != 0 THEN SUM(PANEN) ELSE SUM(NON_PANEN) END AS FACTOR,
    //             ( 
    //               SUM(PANEN) /
    //               (SUM(PANEN) + SUM(NON_PANEN))
                  
    //             ) AS DAILY_PANEN
    //             FROM HITUNG
    //             GROUP BY PLANT_CODE, POSTING_DATE, NIK
                
    //             ORDER BY POSTING_DATE ASC
    //           )
    //         ) ";

    $time_start = microtime(true);
    $result = [];
    try {
      $ps = $this->tapdw->query($sql);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result = floatval($row["PRODUKTIFITAS"]);
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
    }

    $time_end = microtime(true);
    $this->logger->debug('getProduktifitas :'. str_replace('  ', '', $sql), array('benchmark' => $time_end - $time_start) );
    return $result;
  }
  
  public function getOvertime($ba_code, $date)
  {
    $yyyymm = date('Ym', strtotime("-1 month", strtotime(str_replace('-','/', $date))));
    $sql = "SELECT NVL(SUM(OVERTIME),0) OT FROM TR_OVERTIME
            WHERE WERKS = '$ba_code' AND POSTING_DATE BETWEEN TRUNC((TO_DATE('".$date."', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH
            AND LAST_DAY(TRUNC((TO_DATE('".$date."', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH)";
    $result = '';

    $time_start = microtime(true);
    $result = [];
    try {
      $ps = $this->tapdw->query($sql);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result = (isset($row["OT"])) ? floatval($row["OT"]) : (int) 0;
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
    }

    $time_end = microtime(true);
    $this->logger->debug('getOvertime :'. str_replace('  ', '', $sql), array('benchmark' => $time_end - $time_start) );
    return $result;
  }

  public function getOvertimeBasedOnJobcode($ba_code, $date, $job)
  {
    $sql = "SELECT NVL(SUM(OVERTIME),0) OT FROM TR_OVERTIME
            WHERE WERKS = '$ba_code' AND JOB_CODE = '$job'
            AND POSTING_DATE BETWEEN TRUNC((TO_DATE('".$date."', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH
            AND LAST_DAY(TRUNC((TO_DATE('".$date."', 'YYYY-MM-DD')), 'MM') - INTERVAL '1' MONTH)";
    $result = '';

    $time_start = microtime(true);
    $result = [];
    try {
      $ps = $this->tapdw->query($sql);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result = (isset($row["OT"])) ? floatval($row["OT"]) : (int) 0;
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
    }

    $time_end = microtime(true);
    $this->logger->debug('getOvertimeBasedOnJobcode :'. str_replace('  ', '', $sql), array('benchmark' => $time_end - $time_start) );
    return $result;
  }

}
