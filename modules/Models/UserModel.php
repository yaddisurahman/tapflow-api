<?php

namespace Modules\Models;

class UserModel
{

  function __construct($database)
  {
    $this->tapdw = $database;
  }

  public function test()
  {
    $q = "SELECT * from tm_employee_sap where rownum < 3";

    try {
      $ps = $this->tapdw->query($q);
      $ps->execute();

      while($row = $ps->fetch()) {
        $result[] = $row;
      }
    } catch (\Exception $e) {
      $result['error'] = true;
      $result['message'] = $e->getMessage();
      $result['status'] = 500;
    }

    return $result;
  }
}