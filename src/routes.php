<?php
// Routes

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * LDAP Logins
 */
$app->post('/login', function (Request $request, Response $response, $args) {
    $username = $request->getParam('username');
    $password = $request->getParam('password');

    if( !$request->getParam('username') || !$request->getParam('password') ) {
      $response->withHeader('status',400);
      $response->withStatus(400);
      return $response->withJson(['valid' => false, 'message' => 'Username and Password required']);
    } else {
      return $this->UserController->ldap($request, $response, $username, $password);
    }
});

$app->get('/login', function (Request $request, Response $response) {
  $res = $response->withStatus(400)->withJson(['valid' => false, 'message' => 'POST Only']);
  return ($res);
});

$app->get('/login/{any}', function ($request, $response, $args) {
  $res = $response->withStatus(400)->withJson(['valid' => false, 'message' => 'POST Only']);
  return ($res);
});


$app->post('/email', function ($request, $response) {
  return $this->EmailController->sendEmail($request, $response);
});

/**
 * Ultimate router
 */
$app->get('/[{url_encode}]', function ($request, $response) {
  
  $uri = $request->getUri();
  $path = $uri->getPath();
  $path = substr($path, 1);
  // $sub_path = explode('?', base64_decode($path));
  $sub_path = explode('?', base64_decode(str_pad(strtr($path, '-_', '+/'), strlen($path) % 4, '=', STR_PAD_RIGHT)) );
  $path = explode('/', $sub_path[0]);
  // unset($path[0]);

  if(count($path) == 1) {
    $this->logger->error('Controller ini is not defined ', array('path' => substr($uri->getPath(),1)));
    $response->withHeader('Content-type', 'application/json');
    return $response->withStatus(404)->withJson(['error' => true, 'status' => 404, 'message' => 'Request ini not valid']);
  }
 $controller = ucfirst(strtolower($path[1])).'Controller';
 
  if(isset($path[2]) && '' != $path[2]) {
    $method = $path[2];
  } else {
    $method = 'index';
  }
  if($controller == 'LoginController') {
    $method = 'validateUser';
    $controller = 'UserController';
  }

  if(!method_exists($this->{$controller},$method)) {
    $this->logger->error('Controller ini is not defined', array('path' => substr($uri->getPath(),1)));
    return $response->withJson(['error' => true, 'status' => 404, 'message' => 'Request is not valid']);
  }

  $param = [];
  if(isset($sub_path[1])) {
    $arguments = explode( '&', $sub_path[1] );

    foreach ($arguments as $arg) {
      $var = explode('=', $arg);
      if(isset($var[1]))
      $param[$var[0]] = urldecode($var[1]);
    }
  }

  return $this->{$controller}->{$method}($request, $response, $param);
});

