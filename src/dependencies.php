<?php
// DIC configuration

$container = $app->getContainer();


// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    // $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    $logger->pushHandler(new Monolog\Handler\RotatingFileHandler($settings['path'], $settings['max_files'], $settings['level']));
    return $logger;
};

/**
 * Error Handler
 */
// $container['errorHandler'] = function ($c) {
//     return function ($request, $response, $exception) use ($c) {
//         return $c;
//     };
// };

/**
 * Database container
 */
$container['tapdw'] = function ($c) {
  $db = $c->get('settings')['tap_dw'];
  $tns = "
  (DESCRIPTION =
      (ADDRESS_LIST =
        (ADDRESS = (PROTOCOL = TCP)(HOST = ".$db['hostname'].")(PORT = ".$db['dbport']."))
      )
      (CONNECT_DATA =
        (SERVICE_NAME = ".$db['servicename'].")
      )
    )";
  $pdo = new PDO("oci:dbname=".$tns,$db['username'],$db['password']);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
  return $pdo;
};
$container['tapflow'] = function ($c) {
  $db = $c->get('settings')['tapflow'];
  $tns = "
  (DESCRIPTION =
      (ADDRESS_LIST =
        (ADDRESS = (PROTOCOL = TCP)(HOST = ".$db['hostname'].")(PORT = ".$db['dbport']."))
      )
      (CONNECT_DATA =
        (SERVICE_NAME = ".$db['servicename'].")
      )
    )";
  $pdo = new PDO("oci:dbname=".$tns,$db['username'],$db['password']);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
  return $pdo;
}; 

//staging add by yoga
$container['staging'] = function ($c) {
  $db = $c->get('settings')['staging'];
  $tns = "
  (DESCRIPTION =
      (ADDRESS_LIST =
        (ADDRESS = (PROTOCOL = TCP)(HOST = ".$db['hostname'].")(PORT = ".$db['dbport']."))
      )
      (CONNECT_DATA =
        (SERVICE_NAME = ".$db['servicename'].")
      )
    )";
  $pdo = new PDO("oci:dbname=".$tns,$db['username'],$db['password']);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
  return $pdo;
}; 

$container['guzzle'] = function ($c) {
  $guzzle = new \GuzzleHttp\Client();
  return $guzzle;
};

/**
 * Helper Container
 */
$container['query_helper'] = function ($c) {
  return new \Modules\Helpers\QueryHelper();
};
$container['ldap'] = function ($c) {
  return $c->get('settings')['ldap'];
};


// Controller
$container['LdapController'] = function ($c) {
  return new \Modules\Controllers\LdapController($c);
};
$container['UserController'] = function ($c) {
  return new \Modules\Controllers\UserController($c);
};
$container['EmployeeController'] = function ($c) {
  return new \Modules\Controllers\EmployeeController($c);
};
$container['AreaController'] = function ($c) {
  return new \Modules\Controllers\AreaController($c);
};
$container['UtilityController'] = function ($c) {
  return new \Modules\Controllers\UtilityController($c);
};
$container['PurchaseorderController'] = function ($c) {
  return new \Modules\Controllers\PurchaseorderController($c);
};
$container['EmailsenderController'] = function ($c) {
  return new \Modules\Controllers\EmailsenderController($c);
};
$container['MailgeneratorController'] = function ($c) {
  return new \Modules\Controllers\MailgeneratorController($c);
};
$container['AttendanceController'] = function ($c) {
  return new \Modules\Controllers\AttendanceController($c);
};
$container['EmailController'] = function ($c) {
  return new \Modules\Controllers\EmailController($c);
};
