<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        // 'renderer' => [
        //     'template_path' => __DIR__ . '/../templates/',
        // ],

        // Monolog settings
        'logger' => [
            'name' => 'TAP-FLOW',
            'path' => __DIR__ . '/../logs/tapflow-api.log',
            'level' => \Monolog\Logger::DEBUG,
            'max_files' => 3,
        ],

        // Oracle database
        'tap_dw' => [
            'hostname'    => '10.20.1.103',
            'dbport'      => '1521',
            'username'    => 'TAP_DW',
            'servicename' => 'tapdw',
            'password'    => 'tapdw123#',
            'dbscheme'    => 'TAP_DW'
        ],
        'tapflow' => [
            'hostname'    => '10.20.1.109',
            // 'hostname'    => '10.20.1.111',
            'dbport'      => '1521',
            'username'    => 'TAP_FLOW',
            'servicename' => 'tapapps',
            'password'    => 'tap_flow',
            // 'password'    => 'T4pfl0w123',
            'dbscheme'    => 'TAP_FLOW'
        ],
        //staging database :add yoga 
        'staging' => [
            'hostname'    => '10.20.1.248',
            // 'hostname'    => '10.20.1.111',
            'dbport'      => '1521',
            'username'    => 'STAGING',
            'servicename' => 'staging',
            'password'    => 'staging',
            // 'password'    => 'T4pfl0w123',
            'dbscheme'    => 'STAGING'
        ],
        // LDAP Settings
        'ldap' => [
            'ldaphost' => 'ldap.tap-agri.com',
            'ldapport' => '389',
            'admin_username' => 'dms',
            'admin_password' => 'tap123',
            'base_dn' => 'OU=B.Triputra Agro Persada, DC=tap, DC=corp',
            'filter' => ['mail', 'sn', 'cn', 'name', 'title'],
            'result_filter' => 15
        ]
    ],
];
